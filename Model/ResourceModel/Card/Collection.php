<?php

namespace MaxiPago\Payment\Model\ResourceModel\Card;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * @var string
     */
    protected $_eventPrefix = 'maxipago_credit_cards_collection';

    /**
     * @var string
     */
    protected $_eventObject = 'card_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('MaxiPago\Payment\Model\Card', 'MaxiPago\Payment\Model\ResourceModel\Card');
    }
}
