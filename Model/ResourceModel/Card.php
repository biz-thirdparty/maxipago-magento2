<?php

namespace MaxiPago\Payment\Model\ResourceModel;

class Card extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Card constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('maxipago_credit_cards', 'entity_id');
    }
}