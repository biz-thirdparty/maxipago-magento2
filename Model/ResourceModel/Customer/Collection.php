<?php

namespace MaxiPago\Payment\Model\ResourceModel\Customer;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * @var string
     */
    protected $_eventPrefix = 'maxipago_customers_collection';

    /**
     * @var string
     */
    protected $_eventObject = 'customer_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('MaxiPago\Payment\Model\Customer', 'MaxiPago\Payment\Model\ResourceModel\Customer');
    }
}
