<?php

namespace MaxiPago\Payment\Model;

class Customer extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('MaxiPago\Payment\Model\ResourceModel\Customer');
    }
}