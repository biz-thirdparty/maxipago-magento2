<?php

namespace MaxiPago\Payment\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Escaper;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Framework\Locale\Bundle\DataBundle;

class ConfigProviderCheckout2 implements ConfigProviderInterface
{
    /** @var int  */
    const YEARS_RANGE = 20;

    /**
     * @var string
     */
    protected $methodCode = "maxipago_payment_checkout2";

    /**
     * @var \Magento\Payment\Model\MethodInterface
     */
    protected $method;

    /**
     * @var Escaper
     */
    protected $escaper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Locale\ResolverInterface
     */
    protected $localeResolver;

    /**
     * @var \MaxiPago\Payment\Helper\Data
     */
    protected $_maxipagoHelper;

    /**
     * @var \MaxiPago\Payment\Helper\Card
     */
    protected $helperCard;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * ConfigProviderCc constructor.
     * @param PaymentHelper $paymentHelper
     * @param Escaper $escaper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Locale\ResolverInterface $localeResolver
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \MaxiPago\Payment\Helper\Data $maxipagoHelper
     * @param \MaxiPago\Payment\Helper\Card $helperCard
     * @param \Magento\Customer\Model\Session $customerSession
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(
        PaymentHelper $paymentHelper,
        Escaper $escaper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Locale\ResolverInterface $localeResolver,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \MaxiPago\Payment\Helper\Data $maxipagoHelper,
        \MaxiPago\Payment\Helper\Card $helperCard,
        \Magento\Customer\Model\Session $customerSession
    )
    {
        $this->escaper         = $escaper;
        $this->method          = $paymentHelper->getMethodInstance($this->methodCode);
        $this->scopeConfig     = $scopeConfig;
        $this->localeResolver  = $localeResolver;
        $this->_date           = $date;
        $this->_maxipagoHelper = $maxipagoHelper;
        $this->helperCard      = $helperCard;
        $this->customerSession = $customerSession;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        return $this->method->isAvailable() ? [
            'payment' => [
                'maxipago_payment_checkout2' => [
                    'active'         => $this->scopeConfig->getValue("payment/maxipago_payment/maxipago_payment_checkout2/active"),
                    'years'          => $this->getYears(),
                    'months'         => $this->getMonths(),
                    'installments'   => $this->getInstallments(),
                    'tokens'         => $this->getTokens(),
                    'use_saved_card' => $this->getUseSavedCard()
                ],
            ],
        ] : [];
    }

    /**
     * @return array
     */
    protected function getInstallments()
    {
        return $this->_maxipagoHelper->getInstallmentsInformation();
    }

    /**
     * @return array
     */
    protected function getMonths()
    {
        $data  = [];
        $months = (new DataBundle())->get(
            $this->localeResolver->getLocale()
        )['calendar']['gregorian']['monthNames']['format']['wide'];

        foreach ($months as $key => $value) {
            $monthNum   = ++$key < 10 ? '0' . $key : $key;
            $data[$key] = $monthNum . ' - ' . $value;
        }

        return $data;
    }

    /**
     * @return array
     */
    protected function getYears()
    {
        $years = [];
        $first = (int) $this->_date->date('Y');

        for ($index = 0; $index <= self::YEARS_RANGE; $index++) {
            $year = $first + $index;
            $years[$year] = $year;
        }

        return $years;
    }

    /**
     * @return array
     */
    protected function getTokens()
    {
        $cardsCollection = $this->helperCard->getSavedCards($this->customerSession->getCustomer()->getId());

        if (!$cardsCollection->count()) {
            return [];
        }

        $result[0]['value'] = '';
        $result[0]['label'] = 'Selecione...';

        $i = 1;

        foreach ($cardsCollection->getItems() as $item) {
            $result[$i]['value'] = $item->getId();
            $result[$i]['label'] = $item->getDescription() . ' (' . $this->_maxipagoHelper->getBrand($item->getBrand()) . ')';
            $i++;
        }

        return $result;
    }

    /**
     * @return bool
     */
    protected function getUseSavedCard()
    {
        $canSave = $this->scopeConfig->getValue("payment/maxipago_payment/maxipago_payment_checkout2/cc_can_save");

        if (!$canSave) {
            return false;
        }

        $cardsCollection = $this->helperCard->getSavedCards($this->customerSession->getCustomer()->getId());

        if (!$cardsCollection->count()) {
            return false;
        }

        return true;
    }
}
