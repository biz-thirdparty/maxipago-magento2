<?php

namespace MaxiPago\Payment\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Escaper;
use Magento\Payment\Helper\Data as PaymentHelper;

class ConfigProviderTicket implements ConfigProviderInterface
{
    /**
     * @var string
     */
    protected $methodCode = "maxipago_payment_ticket";

    /**
     * @var \Magento\Payment\Model\MethodInterface
     */
    protected $method;

    /**
     * @var Escaper
     */
    protected $escaper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * ConfigProviderTicket constructor.
     * @param PaymentHelper $paymentHelper
     * @param Escaper $escaper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(
        PaymentHelper $paymentHelper,
        Escaper $escaper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->escaper = $escaper;
        $this->method = $paymentHelper->getMethodInstance($this->methodCode);
        $this->scopeConfig = $scopeConfig;

    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->method->isAvailable() ? [
            'payment' => [
                'maxipago_payment_ticket' => [
                    'active' => $this->scopeConfig->getValue("payment/maxipago_payment/maxipago_payment_ticket/active"),
                    'instruction' => $this->getInstruction(),
                    'due'         => $this->getDue(),
                    'banks'       => $this->getBanks()
                ],
            ],
        ] : [];
    }

    /**
     * @return array
     */
    protected function getBanks()
    {
        return explode(',', $this->escaper->escapeHtml($this->scopeConfig->getValue("payment/maxipago_payment/maxipago_payment_ticket/banks")));
    }

    /**
     * @return string
     */
    protected function getInstruction()
    {
        return nl2br($this->scopeConfig->getValue("payment/maxipago_payment/maxipago_payment_ticket/instruction"));
    }

    /**
     * @return string
     */
    protected function getDue()
    {
        $day = (int)$this->scopeConfig->getValue("payment/maxipago_payment/maxipago_payment_ticket/due_days");

        if ($day > 1) {
            return nl2br(sprintf(__('Vencimento em %s dias'), $day));
        } else {
            return nl2br(sprintf(__('Vencimento em %s dia'), $day));
        }
    }
}
