<?php

namespace MaxiPago\Payment\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Escaper;
use Magento\Payment\Helper\Data as PaymentHelper;

class ConfigProviderTef implements ConfigProviderInterface
{
    /**
     * @var string
     */
    protected $methodCode = "maxipago_payment_tef";

    /**
     * @var \Magento\Payment\Model\MethodInterface
     */
    protected $method;

    /**
     * @var Escaper
     */
    protected $escaper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * ConfigProviderTef constructor.
     * @param PaymentHelper $paymentHelper
     * @param Escaper $escaper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(
        PaymentHelper $paymentHelper,
        Escaper $escaper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->escaper = $escaper;
        $this->method = $paymentHelper->getMethodInstance($this->methodCode);
        $this->scopeConfig = $scopeConfig;

    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        return $this->method->isAvailable() ? [
            'payment' => [
                'maxipago_payment_tef' => [
                    'active' => $this->scopeConfig->getValue("payment/maxipago_payment/maxipago_payment_tef/active"),
                    'description' => $this->getDescription(),
                    'banks'       => $this->getBanks()
                ],
            ],
        ] : [];
    }

    /**
     * @return array
     */
    protected function getBanks()
    {
        return explode(',', $this->escaper->escapeHtml($this->scopeConfig->getValue("payment/maxipago_payment/maxipago_payment_tef/banks")));
    }

    /**
     * @return string
     */
    protected function getDescription()
    {
        return nl2br($this->scopeConfig->getValue("payment/maxipago_payment/maxipago_payment_tef/description"));
    }
}