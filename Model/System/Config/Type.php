<?php

namespace MaxiPago\Payment\Model\System\Config;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Environment
 * @package UOL\PagSeguro\Model\System\Config
 */
class Type implements ArrayInterface
{
    /**
     * @return array of options
     */
    public function toOptionArray()
    {
        return [
            'price'    => __('Price'),
            'compound' => __('Compund'),
            'simple'   => __('Simple'),
        ];
    }
}
