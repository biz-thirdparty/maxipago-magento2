<?php
/**
 * 2007-2016 [PagSeguro Internet Ltda.]
 *
 * NOTICE OF LICENSE
 *
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 *http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 *
 * @author    PagSeguro Internet Ltda.
 * @copyright 2016 PagSeguro Internet Ltda.
 * @license   http://www.apache.org/licenses/LICENSE-2.0
 */

/**
 * Used in creating options for Sanbox|Production config value selection
 */

namespace MaxiPago\Payment\Model\System\Config;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Environment
 * @package UOL\PagSeguro\Model\System\Config
 */
class Cron implements ArrayInterface
{
    /** @var string  */
    const FIVE_MINUTES = '*/5 * * * *';

    /** @var string  */
    const HOURLY = '0 */1 * * *';

    /** @var string  */
    const TWO_HOURS = '0 */2 * * *';

    /** @var string  */
    const TWELVE_HOURS = '0 */12 * * *';

    /** @var string  */
    const ONCE_A_DAY = '0 1 * * *';

    /**
     * @return array of options
     */
    public function toOptionArray()
    {
        return [
            self::FIVE_MINUTES => __('Each 5 minutes'),
            self::HOURLY       => __('Hourly'),
            self::TWO_HOURS    => __('Each 2 hours'),
            self::TWELVE_HOURS => __('Each 12 hours'),
            self::ONCE_A_DAY   => __('Once a day')
        ];
    }
}
