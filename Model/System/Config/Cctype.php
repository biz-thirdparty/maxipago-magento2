<?php

namespace MaxiPago\Payment\Model\System\Config;

class Cctype extends \Magento\Payment\Model\Source\Cctype
{
    /**
     * @return array of options
     */
    public function getAllowedTypes()
    {
        return ['VI', 'MC', 'AE', 'DI', 'JCB', 'OT'];
    }
}
