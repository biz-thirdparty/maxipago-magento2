<?php

namespace MaxiPago\Payment\Model\System\Config\Installment;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Environment
 * @package UOL\PagSeguro\Model\System\Config
 */
class Options implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];

        for ($i = 1; $i <= 12; $i++) {
            $result[] = [
                'value' => $i,
                'label' => $i
            ];
        }

        return $result;
    }
}
