<?php

namespace MaxiPago\Payment\Model\System\Config;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Environment
 * @package UOL\PagSeguro\Model\System\Config
 */
class Environment implements ArrayInterface
{
    /**
     * @return array of options
     */
    public function toOptionArray()
    {
        return [
            'TEST' => __('Teste'),
            'LIVE' => __('Produção')
        ];
    }
}
