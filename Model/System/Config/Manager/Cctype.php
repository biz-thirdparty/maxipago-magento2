<?php

namespace MaxiPago\Payment\Model\System\Config\Manager;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Environment
 * @package UOL\PagSeguro\Model\System\Config
 */
class Cctype implements ArrayInterface
{
    /**
     * @return array of options
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'VI',
                'label' => __('Visa')
            ],
            [
                'value' => 'MC',
                'label' => __('Mastercard')
            ],
            [
                'value' => 'AM',
                'label' => __('Amex')
            ],
            [
                'value' => 'DC',
                'label' => __('Diners Club')
            ],
            [
                'value' => 'EL',
                'label' => __('Elo')
            ],
            [
                'value' => 'DI',
                'label' => __('Discover')
            ],
            [
                'value' => 'HC',
                'label' => __('Hipercard')
            ],
            [
                'value' => 'JC',
                'label' => __('JCB')
            ],
            [
                'value' => 'AU',
                'label' => __('Aura')
            ],
            [
                'value' => 'CR',
                'label' => __('Credz')
            ]
        ];
    }

    /**
     * @param $ccType
     * @return array|null
     */
    public function getProcessors($ccType)
    {
        $processors = null;

        switch ($ccType) {
            case 'VI':
                $processors = [
                    [
                        'value' => '1',
                        'label' => __('Simulador de Teste')
                    ],
                    [
                        'value' => '2',
                        'label' => __('Redecard')
                    ],
                    [
                        'value' => '3',
                        'label' => __('GetNet')
                    ],
                    [
                        'value' => '4',
                        'label' => __('Cielo')
                    ],
                    [
                        'value' => '5',
                        'label' => __('e.Rede')
                    ],
                    [
                        'value' => '6',
                        'label' => __('Elavon')
                    ],
                    [
                        'value' => '9',
                        'label' => __('Stone')
                    ],
                    [
                        'value' => '10',
                        'label' => __('bin')
                    ]
                ];

                break;

            case 'MC':
                $processors = [
                    [
                        'value' => '1',
                        'label' => __('Simulador de Teste')
                    ],
                    [
                        'value' => '2',
                        'label' => __('Redecard')
                    ],
                    [
                        'value' => '3',
                        'label' => __('GetNet')
                    ],
                    [
                        'value' => '4',
                        'label' => __('Cielo')
                    ],
                    [
                        'value' => '5',
                        'label' => __('e.Rede')
                    ],
                    [
                        'value' => '6',
                        'label' => __('Elavon')
                    ],
                    [
                        'value' => '9',
                        'label' => __('Stone')
                    ]
                ];

                break;

            case 'AM':
                $processors = [
                    [
                        'value' => '1',
                        'label' => __('Simulador de Teste')
                    ],
                    [
                        'value' => '4',
                        'label' => __('Cielo')
                    ]
                ];

                break;

            case 'DC':
                $processors = [
                    [
                        'value' => '1',
                        'label' => __('Simulador de Teste')
                    ],
                    [
                        'value' => '2',
                        'label' => __('Redecard')
                    ],
                    [
                        'value' => '4',
                        'label' => __('Cielo')
                    ],
                    [
                        'value' => '5',
                        'label' => __('e.Rede')
                    ],
                    [
                        'value' => '6',
                        'label' => __('Elavon')
                    ]
                ];

                break;

            case 'EL':
                $processors = [
                    [
                        'value' => '1',
                        'label' => __('Simulador de Teste')
                    ],
                    [
                        'value' => '4',
                        'label' => __('Cielo')
                    ]
                ];

                break;

            case 'DI':
                $processors = [
                    [
                        'value' => '1',
                        'label' => __('Simulador de Teste')
                    ],
                    [
                        'value' => '2',
                        'label' => __('Redecard')
                    ],
                    [
                        'value' => '4',
                        'label' => __('Cielo')
                    ],
                    [
                        'value' => '5',
                        'label' => __('e.Rede')
                    ],
                    [
                        'value' => '6',
                        'label' => __('Elavon')
                    ]
                ];

                break;

            case 'HC':
                $processors = [
                    [
                        'value' => '1',
                        'label' => __('Simulador de Teste')
                    ],
                    [
                        'value' => '2',
                        'label' => __('Redecard')
                    ],
                    [
                        'value' => '5',
                        'label' => __('e.Rede')
                    ]
                ];

                break;

            case 'JC':
                $processors = [
                    [
                        'value' => '1',
                        'label' => __('Simulador de Teste')
                    ],
                    [
                        'value' => '4',
                        'label' => __('Cielo')
                    ],
                    [
                        'value' => '2',
                        'label' => __('Redecard')
                    ],
                    [
                        'value' => '5',
                        'label' => __('e.Rede')
                    ]
                ];

                break;

            case 'AU':
                $processors = [
                    [
                        'value' => '1',
                        'label' => __('Simulador de Teste')
                    ],
                    [
                        'value' => '4',
                        'label' => __('Cielo')
                    ]
                ];

                break;

            case 'CR':
                $processors = [
                    [
                        'value' => '1',
                        'label' => __('Simulador de Teste')
                    ]
                ];

                break;

            default:
                $processors = [];
                break;
        }

        return $processors;
    }
}
