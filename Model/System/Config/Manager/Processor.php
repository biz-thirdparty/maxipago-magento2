<?php

namespace MaxiPago\Payment\Model\System\Config\Manager;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Environment
 * @package UOL\PagSeguro\Model\System\Config
 */
class Processor implements ArrayInterface
{
    /**
     * @return array of options
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => '1',
                'label' => __('Test Processor')
            ],
            [
                'value' => '2',
                'label' => __('Redecard')
            ],
            [
                'value' => '3',
                'label' => __('GetNet')
            ],
            [
                'value' => '4',
                'label' => __('Cielo')
            ],
            [
                'value' => '5',
                'label' => __('e.Rede')
            ],
            [
                'value' => '6',
                'label' => __('Elavon')
            ],
            [
                'value' => '9',
                'label' => __('Stone')
            ]
        ];
    }
}