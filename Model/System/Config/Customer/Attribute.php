<?php

namespace MaxiPago\Payment\Model\System\Config\Customer;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Banks
 * @package MaxiPago\Payment\Model\System\Config\Eft
 */
class Attribute implements ArrayInterface
{
    protected $objectManager;

    public function __construct(
        //\Magento\Customer\Model\Customer $customerFactory,
        \Magento\Framework\ObjectManagerInterface $interface
    )
    {
        // $this->customerFactory = $customerFactory;
        $this->objectManager = $interface;
    }

    /**
     * @return array of options
     */
    public function toOptionArray()
    {
        $customerAttributes = $this->objectManager->get('Magento\Customer\Model\Customer')->getAttributes();

        $result = [];

        foreach ($customerAttributes as $attribute) {
            $result[] = [
                'value' => $attribute->getAttributeCode(),
                'label' => $attribute->getFrontend()->getLabel() ?: $attribute->getAttributeCode()
            ];
        }

        return $result;
    }
}
