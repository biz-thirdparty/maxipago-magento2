<?php

namespace MaxiPago\Payment\Model\System\Config\Fraud;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Environment
 * @package UOL\PagSeguro\Model\System\Config
 */
class Processor implements ArrayInterface
{
    /**
     * @return array of options
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => '',
                'label' => 'Default'
            ],
            [
                'value' => 98,
                'label' => 'ClearSale Total'
            ],
            [
                'value' => 99,
                'label' => 'Kount'
            ]
        ];
    }
}
