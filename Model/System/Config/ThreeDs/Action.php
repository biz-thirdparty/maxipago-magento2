<?php

namespace MaxiPago\Payment\Model\System\Config\ThreeDs;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Environment
 * @package UOL\PagSeguro\Model\System\Config
 */
class Action implements ArrayInterface
{
    /**
     * @return array of options
     */
    public function toOptionArray()
    {
        return [
            'decline'  => __('Stop Processing'),
            'continue' => __('Continue'),
        ];
    }
}
