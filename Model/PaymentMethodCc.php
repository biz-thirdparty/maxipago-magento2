<?php

namespace MaxiPago\Payment\Model;

use Magento\Framework\UrlInterface;
use \Magento\Payment\Model\Method\AbstractMethod;
use Magento\Persistent\Observer\PreventClearCheckoutSessionObserver;
use Magento\Sales\Model\Order;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Sales\Model\Order\Payment;

class PaymentMethodCc extends \Magento\Payment\Model\Method\Cc
{
    /** @var int  */
    const ROUND_UP = 100;

    /** @var string  */
    const DEFAULT_IP = '127.0.0.1';

    /** @var int  */
    const DEFAULT_TICKET_BANK = 12;

    /** @var int  */
    const DEFAULT_EFT_BANK = 17;

    /** @var int  */
    const DEFAULT_REDEPAY_BANK = 18;

    /**
     * @var bool
     */
    protected $_canAuthorize = true;

    /**
     * @var bool
     */
    protected $_canCapture = true;

    /**
     * @var bool
     */
    protected $_canRefund = true;

    /**
     * @var string
     */
    protected $_code = 'maxipago_payment_cc';

    /**
     * @var bool
     */
    protected $_isGateway = true;

    /**
     * @var bool
     */
    protected $_canCapturePartial = true;

    /**
     * @var bool
     */
    protected $_canRefundInvoicePartial = true;

    /**
     * @var bool
     */
    protected $_canVoid = true;

    /**
     * @var bool
     */
    protected $_canCancel = true;

    /**
     * @var bool
     */
    protected $_canUseForMultishipping = false;

    /**
     * @var bool
     */
    protected $_canUseInternal = false;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $_countryFactory;

    /**
     * @var array
     */
    protected $_supportedCurrencyCodes = ['BRL'];

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    /**
     * @var \MaxiPago\Payment\Helper\Data
     */
    protected $_maxipagoHelper;

    /**
     * @var \MaxiPago\Payment\Helper\Customer
     */
    protected $_maxipagoCustomerHelper;

    /**
     * @var \MaxiPago\Payment\Helper\Order
     */
    protected $_maxipagoOrderHelper;

    /**
     * @var string
     */
    protected $_infoBlockType = 'MaxiPago\Payment\Block\Info\Ticket';

    /**
     * @var \MaxiPago\Payment\lib\MaxiPago
     */
    protected $_libMaxiPago;

    /**
     * @var \Magento\Framework\Session\SessionManager
     */
    protected $session;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \MaxiPago\Payment\Helper\Card
     */
    protected $cardHelper;

    /**
     * PaymentMethodCc constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param \Magento\Framework\Module\ModuleListInterface $moduleList
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \MaxiPago\Payment\Helper\Data $maxipagoHelper
     * @param \MaxiPago\Payment\Helper\Customer $maxipagoCustomerHelper
     * @param \MaxiPago\Payment\Helper\Order $maxipagoOrderHelper
     * @param \MaxiPago\Payment\lib\MaxiPago $libMaxiPago
     * @param \Magento\Framework\Session\SessionManager $sessionManager
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \MaxiPago\Payment\Helper\Card $cardHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Checkout\Model\Cart $cart,
        \MaxiPago\Payment\Helper\Data $maxipagoHelper,
        \MaxiPago\Payment\Helper\Customer $maxipagoCustomerHelper,
        \MaxiPago\Payment\Helper\Order $maxipagoOrderHelper,
        \MaxiPago\Payment\lib\MaxiPago $libMaxiPago,
        \Magento\Framework\Session\SessionManager $sessionManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \MaxiPago\Payment\Helper\Card $cardHelper,
        array $data = []
    )
    {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $moduleList,
            $localeDate,
            null,
            null,
            $data
        );

        $this->_countryFactory         = $countryFactory;
        $this->scopeConfig             = $scopeConfig;
        $this->_cart                   = $cart;
        $this->_maxipagoHelper         = $maxipagoHelper;
        $this->_maxipagoCustomerHelper = $maxipagoCustomerHelper;
        $this->_maxipagoOrderHelper    = $maxipagoOrderHelper;
        $this->_libMaxiPago            = $libMaxiPago;
        $this->session                 = $sessionManager;
        $this->_registry               = $registry;
        $this->checkoutSession         = $checkoutSession;
        $this->cardHelper              = $cardHelper;
    }

    /**
     * @param \Magento\Framework\DataObject $data
     * @return $this|\Magento\Payment\Model\Method\Cc
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function assignData(\Magento\Framework\DataObject $data)
    {
        parent::assignData($data);

        $infoInstance = $this->getInfoInstance();
        $currentData = $data->getAdditionalData();

        $quote = $this->checkoutSession->getQuote();

        $ccCid        = preg_replace("/[^0-9]/", '', isset($currentData['cc_cid']) ? $currentData['cc_cid'] : null);
        $installments = isset($currentData['installments']) ? $currentData['installments'] : null;
        $token        = isset($currentData['token']) ? $currentData['token'] : null;
        $grandTotal   = $quote->getBaseGrandTotal();
        $hasInterest  = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/installment_type');

        $cpfCnpj = isset($currentData['cpf_cnpj']) ? $currentData['cpf_cnpj'] : null;

        /*
        if (!$this->scopeConfig->getValue('payment/maxipago_payment/show_taxvat_field')) {
            $cpfCnpj = $this->_maxipagoHelper->getTaxvatValue();
        }
        */

        if ($token) {
            $card = $this->cardHelper->getSavedCard($token);

            $infoInstance->setAdditionalInformation('cc_token', $card->getToken());
            $infoInstance->setAdditionalInformation('cc_description', $card->getDescription());
            $infoInstance->setAdditionalInformation('cc_customer_id_maxipago', $card->getCustomerIdMaxipago());

            $ccType = isset($currentData['cc_type']) ? $currentData['cc_type'] : null;
            $ccCid  = isset($currentData['cc_cid'])  ? $currentData['cc_cid']  : null;
        } else {
            $ccType      = isset($currentData['cc_type']) ? $currentData['cc_type'] : null;
            $ccOwner     = isset($currentData['cc_owner']) ? $currentData['cc_owner'] : null;
            $ccNumber    = preg_replace("/[^0-9]/", '', isset($currentData['cc_number']) ? $currentData['cc_number'] : null);
            $ccNumberEnc = $infoInstance->encrypt($ccNumber);
            $ccLast4     = substr($ccNumber, -4);
            $ccExpMonth  = str_pad($currentData['month'], 2, '0', STR_PAD_LEFT);
            $ccExpYear   = isset($currentData['year']) ? $currentData['year'] : null;
            $saveCard    = isset($currentData['save_card']) ? $currentData['cc_type'] : null;
            $ccCid       = isset($currentData['cc_cid']) ? $currentData['cc_cid'] : null;

            $infoInstance->setCcOwner($ccOwner);
            $infoInstance->setCcNumber($ccNumberEnc);
            $infoInstance->setCcExpMonth($ccExpMonth);
            $infoInstance->setCcExpYear($ccExpYear);
            $infoInstance->setCcNumberEnc($ccNumberEnc);
            $infoInstance->setCcLast4($ccLast4);

            $infoInstance->setAdditionalInformation('cc_token', false);
            $infoInstance->setAdditionalInformation('cc_description', false);
            $infoInstance->setAdditionalInformation('cc_customer_id_maxipago', false);
            $infoInstance->setAdditionalInformation('cc_save_card', $saveCard);
        }

        $infoInstance->setAdditionalInformation('cpf_cnpj', $cpfCnpj);

        $interestRate = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/interest_rate');
        $installmentsWithoutInterest = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/installments_without_interest_rate');

        if ($installmentsWithoutInterest >= $installments) {
            $interestRate = null;
        }

        if ($installments > 1) {
            $installmentsValue = $this->_maxipagoHelper->getInstallmentValue($grandTotal, $installments);
            $totalOrderWithInterest = $installmentsValue * $installments;
            $interestValue = $totalOrderWithInterest - $grandTotal;

            $infoInstance->setAdditionalInformation('cc_interest_amount', $interestValue);
            $infoInstance->setAdditionalInformation('cc_total_with_interest', $totalOrderWithInterest);
            $infoInstance->setAdditionalInformation('cc_interest_value', $this->_maxipagoHelper->getInstallmentValue($grandTotal, $installments));
        }

        $infoInstance->setAdditionalInformation('cc_cid', $ccCid);
        $infoInstance->setAdditionalInformation('cc_has_interest', $hasInterest);
        $infoInstance->setAdditionalInformation('cc_interest_rate', $interestRate);
        $infoInstance->setAdditionalInformation('cc_installment_value', $this->_maxipagoHelper->getInstallmentValue($grandTotal, $installments));
        $infoInstance->setAdditionalInformation('cc_installments', $installments);
        $infoInstance->setAdditionalInformation('installments', $installments);
        $infoInstance->setAdditionalInformation('base_grand_total', $grandTotal);

        $infoInstance->setCcType($ccType);
        $infoInstance->setCcInstallments($installments);

        $this->_registry->unregister('maxipago_cc_cid');
        $this->_registry->register('maxipago_cc_cid', $ccCid);

        return $this;
    }

    /**
     * @return $this|\Magento\Payment\Model\Method\Cc
     */
    public function validate()
    {
        return $this;
    }

    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return $this|\Magento\Payment\Model\Method\Cc
     * @throws LocalizedException
     */
    public function order(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        try {
            $softDescriptor = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/soft_descriptor');
            $processingType = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/cc_payment_action');

            $order    = $payment->getOrder();
            $response = null;

            $orderId    = $order->getIncrementId();
            $fraudCheck = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/fraud_check') ? 'Y' : 'N';

            $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();
            $currencysymbol = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
            $currencyCode   = $currencysymbol->getStore()->getCurrentCurrencyCode();

            $use3DS = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/use_3ds');

            $ccCid = $payment->getAdditionalInformation('cc_cid');

            $ccInstallments = $payment->getAdditionalInformation('installments');
            $ipAddress      = $this->getIpAddress();
            $hasInterest    = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/installment_type');

            if ($hasInterest == 'N' && $payment->getAdditionalInformation('cc_total_with_interest') > 0) {
                $amount = number_format($payment->getAdditionalInformation('cc_total_with_interest'), 2, '.', '');
            }

            $token  = $payment->getAdditionalInformation('cc_token');
            $amount = number_format($amount, 2, '.', '');

            $ccBrand     = $payment->getCcType();
            $processorID = $this->_maxipagoHelper->getProcessor($ccBrand);

            if ($token) {
                $customerIdMp = $payment->getAdditionalInformation('cc_customer_id_maxipago');

                $data = array(
                    'customerId'           => $customerIdMp,
                    'token'                => $token,
                    'cvvNumber'            => $ccCid,
                    'referenceNum'         => $orderId,
                    'processorID'          => $processorID,
                    'ipAddress'            => $ipAddress,
                    'fraudCheck'           => $fraudCheck,
                    'currencyCode'         => $currencyCode,
                    'chargeTotal'          => $amount,
                    'numberOfInstallments' => $ccInstallments,
                    'chargeInterest'       => $hasInterest,
                    'softDescriptor'       => $softDescriptor
                );

            } else {
                $ccOwner    = $payment->getAdditionalInformation('cc_owner');
                $ccNumber   = $payment->decrypt($payment->getCcNumber());
                $ccExpMonth = str_pad($payment->getCcExpMonth(), 2, '0', STR_PAD_LEFT);
                $ccExpYear  = $payment->getCcExpYear();

                $data = array(
                    'referenceNum'         => $orderId,
                    'processorID'          => $processorID,
                    'ipAddress'            => $ipAddress,
                    'fraudCheck'           => $fraudCheck,
                    'number'               => $ccNumber,
                    'expMonth'             => $ccExpMonth,
                    'expYear'              => $ccExpYear,
                    'cvvNumber'            => $ccCid,
                    'currencyCode'         => $currencyCode,
                    'chargeTotal'          => $amount,
                    'numberOfInstallments' => $ccInstallments,
                    'chargeInterest'       => $hasInterest,
                    'softDescriptor'       => $softDescriptor
                );

                $ccSaveCard = $payment->getAdditionalInformation('cc_save_card');

                if ($ccSaveCard) {
                    $this->cardHelper->saveCard($payment);
                }
            }

            $merchantId  = $this->scopeConfig->getValue('payment/maxipago_payment/id');
            $merchantKey = $this->scopeConfig->getValue('payment/maxipago_payment/store_key');
            $merchantSecret = $this->scopeConfig->getValue('payment/maxipago_payment/secret_store_key');

            if ($merchantId && $merchantSecret) {
                $environment = $this->scopeConfig->getValue('payment/maxipago_payment/test_environment');
                $this->_libMaxiPago->setCredentials($merchantId, $merchantKey);
                $this->_libMaxiPago->setEnvironment($environment);
            }

            if ($fraudCheck == 'Y' && $processingType == 'auth') {
                $fraudProcessorId = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/fraud_processor');

                if ($fraudProcessorId) {
                    $captureOnLowRisk = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/capture_on_low_risk') ? 'Y' : 'N';
                    $voidOnHighRisk   = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/void_on_high_risk') ? 'Y' : 'N';

                    $data['fraudProcessorID'] = $fraudProcessorId;
                    $data['captureOnLowRisk'] = $captureOnLowRisk;
                    $data['voidOnHighRisk']   = $voidOnHighRisk;

                    if ($fraudProcessorId == '98') {
                        $sessionId = $this->session->getSessionId();
                        $data['fraudToken'] = $sessionId;
                    } else if ($fraudProcessorId == '99') {
                        $hash = hash_hmac('md5', $merchantId . '*' . $orderId, $merchantSecret);
                        $data['fraudToken'] = $hash;
                    }

                    $data['websiteId'] = 'DEFAULT';
                }
            }

            if ($use3DS) {
                $data['mpiProcessorID'] = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/mpi_processor');
                $data['onFailure'] = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/failure_action');
            }

            $billingData  = $this->_maxipagoCustomerHelper->getAddressData($order->getCustomerId(), $payment);
            $shippingData = $this->_maxipagoCustomerHelper->getAddressData($order->getCustomerId(), $payment, 'shipping');

            $orderData = $this->_maxipagoOrderHelper->getOrderData($payment);

            $data = array_merge($data, $billingData, $shippingData, $orderData);

            if ($processingType == 'auth') {
                if ($use3DS) {
                    $this->_libMaxiPago->authCreditCard3DS($data);
                } else {
                    $this->_libMaxiPago->creditCardAuth($data);
                }
            } else {
                if ($use3DS) {
                    $this->_libMaxiPago->saleCreditCard3DS($data);
                } else {
                    $this->_libMaxiPago->creditCardSale($data);
                }
            }

            $response = $this->_libMaxiPago->response;

            $this->_maxipagoHelper->debug($this->_libMaxiPago);

            if (isset($response['transactionID']) && $response['transactionID']) {

                $tid = $response['transactionID'];
                $payment->setCcTransId($tid);

                $payment = $this->setAdditionalInfo($payment, $response);

                if ($response['responseCode'] != 0 && $response['responseCode'] != 5) {

                    if ($this->scopeConfig->getValue('payment/maxipago_payment/stop_processing')) {
                        throw new \Exception('The transaction wasn\'t authorized by the issuer, please check your data and try again');
                    }

                    $payment->setSkipOrderProcessing(true);
                } else {

                    if (isset($response['authenticationURL'])) {
                        $this->_registry->register('maxipago_redirect_url', $response['authenticationURL']);
                    }

                }
            } else {
                throw new \Exception('There was an error processing your request. Please contact us or try again later.');
            }
        } catch (\Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }

        return $this;
    }

    /**
     * @param \Magento\Quote\Api\Data\CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        if (!$this->isActive($quote ? $quote->getStoreId() : null)) {
            return false;
        }
        return true;
    }

    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param $response
     * @return \Magento\Payment\Model\InfoInterface
     */
    protected function setAdditionalInfo(\Magento\Payment\Model\InfoInterface $payment, $response)
    {
        if (isset($response['transactionID'])) {
            $tid = $response['transactionID'];
            $payment->setAdditionalInformation('transaction_id', $tid);
            $payment->setTransactionId($tid);
        }

        if (isset($response['processorTransactionID'])) {
            $payment->setAdditionalInformation('processor_transaction_id', $response['processorTransactionID']);
        }

        if (isset($response['processorReferenceNumber'])) {
            $payment->setAdditionalInformation('processor_reference_number', $response['processorReferenceNumber']);
        }

        if (isset($response['creditCardScheme'])) {
            $payment->setAdditionalInformation('credit_card_scheme', $response['creditCardScheme']);
        }

        if (isset($response['creditCardCountry'])) {
            $payment->setAdditionalInformation('credit_card_scheme', $response['creditCardCountry']);
        }

        if (isset($response['responseMessage'])) {
            $payment->setAdditionalInformation('response_message', $response['responseMessage']);
        }

        if (isset($response['responseCode'])) {
            $payment->setAdditionalInformation('response_code', $response['responseCode']);
        }

        if (isset($response['authCode'])) {
            $payment->setAdditionalInformation('auth_code', $response['authCode']);
        }

        if (isset($response['orderID'])) {
            $payment->setAdditionalInformation('order_id', $response['orderID']);
        }

        if (isset($response['fraudScore'])) {
            $payment->setAdditionalInformation('fraud_score', $response['fraudScore']);
        }

        if (isset($response['transactionState'])) {
            $payment->setAdditionalInformation('last_transaction_state', $response['transactionState']);
        }

        if (isset($response['authenticationURL'])) {
            $payment->setAdditionalInformation('authentication_url', $response['authenticationURL']);
        }

        if (isset($response['boletoUrl'])) {
            $payment->setAdditionalInformation('boleto_url', $response['boletoUrl']);
        }

        if (isset($response['onlineDebitUrl'])) {
            $payment->setAdditionalInformation('boleto_url', $response['onlineDebitUrl']);
        }

        if (isset($response['creditCardCountry'])) {
            $payment->setAdditionalInformation('cc_country', $response['creditCardCountry']);
        }

        if (isset($response['creditCardScheme'])) {
            $payment->setAdditionalInformation('cc_scheme', $response['creditCardScheme']);
        }

        if (isset($response['result']) && isset($response['result']['pay_order_id'])) {
            $payment->setAdditionalInformation('pay_order_id', $response['result']['pay_order_id']);
        }

        return $payment;
    }

    /**
     * @param $ipAddress
     * @return string
     */
    private function validateIP($ipAddress)
    {
        if (filter_var($ipAddress, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            return $ipAddress;
        }

        return self::DEFAULT_IP;
    }

    /**
     * @return string
     */
    private function getIpAddress()
    {
        $om  = \Magento\Framework\App\ObjectManager::getInstance();
        $obj = $om->get('Magento\Framework\HTTP\PhpEnvironment\RemoteAddress');
        $ip  = $obj->getRemoteAddress();

        $ipAddress = $ip ? $this->validateIP($ip) : self::DEFAULT_IP;
        return $ipAddress;
    }
}