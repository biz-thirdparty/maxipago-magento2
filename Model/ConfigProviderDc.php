<?php

namespace MaxiPago\Payment\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Escaper;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Framework\Locale\Bundle\DataBundle;

class ConfigProviderDc implements ConfigProviderInterface
{
    /** @var int  */
    const YEARS_RANGE = 20;

    /** @var string  */
    protected $methodCode = "maxipago_payment_dc";

    /**
     * @var \Magento\Payment\Model\MethodInterface
     */
    protected $method;

    /**
     * @var Escaper
     */
    protected $escaper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Locale\ResolverInterface
     */
    protected $localeResolver;

    /**
     * @var \MaxiPago\Payment\Helper\Data
     */
    protected $_maxipagoHelper;

    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $assetRepository;

    /**
     * @var array
     */
    private $brands = [
        'VI' => 'visa',
        'MC' => 'master',
        'AM' => 'amex',
        'DC' => 'diners-club',
        'EL' => 'elo',
        'DI' => 'discover',
        'HC' => 'hipercard',
        'JC' => 'jcb',
        'AU' => 'aura',
        'CR' => 'credz'
    ];

    /**
     * ConfigProviderDc constructor.
     * @param PaymentHelper $paymentHelper
     * @param Escaper $escaper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Locale\ResolverInterface $localeResolver
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \MaxiPago\Payment\Helper\Data $maxipagoHelper
     * @param \Magento\Framework\View\Asset\Repository $assetRepository
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(
        PaymentHelper $paymentHelper,
        Escaper $escaper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Locale\ResolverInterface $localeResolver,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \MaxiPago\Payment\Helper\Data $maxipagoHelper,
        \Magento\Framework\View\Asset\Repository $assetRepository
    )
    {
        $this->escaper         = $escaper;
        $this->method          = $paymentHelper->getMethodInstance($this->methodCode);
        $this->scopeConfig     = $scopeConfig;
        $this->localeResolver  = $localeResolver;
        $this->_date           = $date;
        $this->_maxipagoHelper = $maxipagoHelper;
        $this->assetRepository = $assetRepository;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->method->isAvailable() ? [
            'payment' => [
                'maxipago_payment_dc' => [
                    'active'       => $this->scopeConfig->getValue("payment/maxipago_payment/maxipago_payment_dc/active"),
                    'years'        => $this->getYears(),
                    'months'       => $this->getMonths(),
                    'icons'        => $this->getIcons(),
                    'active_icons' => $this->getActiveIcons()
                ],
            ],
        ] : [];
    }

    /**
     * @return mixed
     */
    protected function getActiveIcons()
    {
        $processors = json_decode($this->scopeConfig->getValue("payment/maxipago_payment/maxipago_payment_cc/processors"), true);

        $result = null;

        foreach ($processors as $processor) {
            $result[$this->brands[$processor['brand']]] = true;
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function getIcons()
    {
        return [
            'amex'        => $this->assetRepository->createAsset('MaxiPago_Payment::img/amex.svg')->getUrl(),
            'aura'        => $this->assetRepository->createAsset('MaxiPago_Payment::img/aura.svg')->getUrl(),
            'credz'       => $this->assetRepository->createAsset('MaxiPago_Payment::img/credz.svg')->getUrl(),
            'diners'      => $this->assetRepository->createAsset('MaxiPago_Payment::img/diners.svg')->getUrl(),
            'diners_club' => $this->assetRepository->createAsset('MaxiPago_Payment::img/diners-club.svg')->getUrl(),
            'discover'    => $this->assetRepository->createAsset('MaxiPago_Payment::img/discover.svg')->getUrl(),
            'elo'         => $this->assetRepository->createAsset('MaxiPago_Payment::img/elo.svg')->getUrl(),
            'hiper'       => $this->assetRepository->createAsset('MaxiPago_Payment::img/hiper.svg')->getUrl(),
            'hipercard'   => $this->assetRepository->createAsset('MaxiPago_Payment::img/hipercard.svg')->getUrl(),
            'jbs'         => $this->assetRepository->createAsset('MaxiPago_Payment::img/jbs.svg')->getUrl(),
            'jcb'         => $this->assetRepository->createAsset('MaxiPago_Payment::img/jcb.svg')->getUrl(),
            'master'      => $this->assetRepository->createAsset('MaxiPago_Payment::img/master.svg')->getUrl(),
            'visa'        => $this->assetRepository->createAsset('MaxiPago_Payment::img/visa.svg')->getUrl()
        ];
    }

    /**
     * @return array
     */
    protected function getMonths()
    {
        $data  = [];
        $months = (new DataBundle())->get(
            $this->localeResolver->getLocale()
        )['calendar']['gregorian']['monthNames']['format']['wide'];

        foreach ($months as $key => $value) {
            $monthNum   = ++$key < 10 ? '0' . $key : $key;
            $data[$key] = $monthNum . ' - ' . $value;
        }

        return $data;
    }

    /**
     * @return array
     */
    protected function getYears()
    {
        $years = [];
        $first = (int) $this->_date->date('Y');

        for ($index = 0; $index <= self::YEARS_RANGE; $index++) {
            $year         = $first + $index;
            $years[$year] = $year;
        }

        return $years;
    }
}
