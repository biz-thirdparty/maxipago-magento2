<?php

namespace MaxiPago\Payment\Model;

use Magento\Framework\UrlInterface;
use \Magento\Payment\Model\Method\AbstractMethod;
use Magento\Sales\Model\Order;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Sales\Model\Order\Payment;

class PaymentMethodCheckout2 extends \Magento\Payment\Model\Method\Cc
{
    /** @var int  */
    const ROUND_UP = 100;

    /** @var string  */
    const DEFAULT_IP = '127.0.0.1';

    /** @var int  */
    const DEFAULT_TICKET_BANK = 12;

    /** @var int  */
    const DEFAULT_EFT_BANK = 17;

    /** @var int  */
    const DEFAULT_REDEPAY_BANK = 18;

    /**
     * @var bool
     */
    protected $_canAuthorize = true;

    /**
     * @var bool
     */
    protected $_canCapture = true;

    /**
     * @var bool
     */
    protected $_canRefund = true;

    /**
     * @var string
     */
    protected $_code = 'maxipago_payment_checkout2';

    /**
     * @var bool
     */
    protected $_isGateway = true;

    /**
     * @var bool
     */
    protected $_canCapturePartial = true;

    /**
     * @var bool
     */
    protected $_canRefundInvoicePartial = true;

    /**
     * @var bool
     */
    protected $_canVoid = true;

    /**
     * @var bool
     */
    protected $_canCancel = true;

    /**
     * @var bool
     */
    protected $_canUseForMultishipping = false;

    /**
     * @var bool
     */
    protected $_canUseCheckout = false;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $_countryFactory;

    /**
     * @var array
     */
    protected $_supportedCurrencyCodes = ['BRL'];

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    /**
     * @var \MaxiPago\Payment\Helper\Data
     */
    protected $_maxipagoHelper;

    /**
     * @var \MaxiPago\Payment\Helper\Customer
     */
    protected $_maxipagoCustomerHelper;

    /**
     * @var \MaxiPago\Payment\Helper\Order
     */
    protected $_maxipagoOrderHelper;

    /**
     * @var string
     */
    protected $_infoBlockType = 'MaxiPago\Payment\Block\Info\Ticket';

    /**
     * @var \MaxiPago\Payment\lib\MaxiPago
     */
    protected $_libMaxiPago;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \MaxiPago\Payment\Logger\Logger
     */
    protected $maxipagoLogger;

    /**
     * @var \MaxiPago\Payment\Helper\Api
     */
    protected $helperApi;

    /**
     * PaymentMethodCheckout2 constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param \Magento\Framework\Module\ModuleListInterface $moduleList
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \MaxiPago\Payment\Helper\Data $maxipagoHelper
     * @param \MaxiPago\Payment\Helper\Customer $maxipagoCustomerHelper
     * @param \MaxiPago\Payment\Helper\Order $maxipagoOrderHelper
     * @param \MaxiPago\Payment\lib\MaxiPago $libMaxiPago
     * @param \Psr\Log\LoggerInterface $_logger
     * @param \MaxiPago\Payment\Logger\Logger $maxipagoLogger
     * @param \MaxiPago\Payment\Helper\Api $helperApi
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Checkout\Model\Cart $cart,
        \MaxiPago\Payment\Helper\Data $maxipagoHelper,
        \MaxiPago\Payment\Helper\Customer $maxipagoCustomerHelper,
        \MaxiPago\Payment\Helper\Order $maxipagoOrderHelper,
        \MaxiPago\Payment\lib\MaxiPago $libMaxiPago,
        \Psr\Log\LoggerInterface $_logger,
        \MaxiPago\Payment\Logger\Logger $maxipagoLogger,
        \MaxiPago\Payment\Helper\Api $helperApi,
        array $data = []
    )
    {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $moduleList,
            $localeDate,
            null,
            null,
            $data
        );

        $this->_countryFactory         = $countryFactory;
        $this->scopeConfig             = $scopeConfig;
        $this->_cart                   = $cart;
        $this->_maxipagoHelper         = $maxipagoHelper;
        $this->_maxipagoCustomerHelper = $maxipagoCustomerHelper;
        $this->_maxipagoOrderHelper    = $maxipagoOrderHelper;
        $this->_libMaxiPago            = $libMaxiPago;
        $this->_logger                 = $logger;
        $this->maxipagoLogger          = $maxipagoLogger;
        $this->helperApi               = $helperApi;
    }

    /**
     * @param \Magento\Framework\DataObject $data
     * @return $this|\Magento\Payment\Model\Method\Cc
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function assignData(\Magento\Framework\DataObject $data)
    {
        parent::assignData($data);

        $infoInstance = $this->getInfoInstance();
        $currentData  = $data->getAdditionalData();

        $installments = isset($currentData['installments']) ? $currentData['installments'] : null;
        $grandTotal   = isset($currentData['base_grand_total']) ? $currentData['base_grand_total'] : null;
        $hasInterest  = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_checkout_2/installment_type');

        $description = isset($currentData['description']) ? $currentData['description'] : null;
        $comments    = isset($currentData['comments'])    ? $currentData['comments']    : null;
        $subject     = isset($currentData['subject'])     ? $currentData['subject']     : null;

        $cpfCnpj = isset($currentData['cpf_cnpj']) ? $currentData['cpf_cnpj'] : null;

        /*
        if (!$this->scopeConfig->getValue('payment/maxipago_payment/show_taxvat_field')) {
            $cpfCnpj = $this->_maxipagoHelper->getTaxvatValue();
        }
        */

        $info = $this->getInfoInstance();

        $info->setAdditionalInformation('description', $description);
        $info->setAdditionalInformation('comments', $comments);
        $info->setAdditionalInformation('subject', $subject);

        $interestRate = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_checkout_2/interest_rate');
        $installmentsWithoutInterest = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_checkout_2/installments_without_interest_rate');

        if ($installmentsWithoutInterest >= $installments) {
            $interestRate = null;
        }

        if ($installments > 1) {
            $installmentsValue = $this->_maxipagoHelper->getInstallmentValue($grandTotal, $installments, 'maxipago_payment_checkout2');
            $totalOrderWithInterest = $installmentsValue * $installments;
            $interestValue = $totalOrderWithInterest - $grandTotal;

            $info->setAdditionalInformation('interest_amount', $interestValue);
            $info->setAdditionalInformation('interest_rate', $interestRate);
            $info->setAdditionalInformation('total_with_interest', $totalOrderWithInterest);
        }

        $info->setAdditionalInformation('cpf_cnpj', $cpfCnpj);
        $info->setAdditionalInformation('has_interest', $hasInterest);
        $info->setAdditionalInformation('interest_rate', $interestRate);
        $info->setAdditionalInformation('installment_value', $this->_maxipagoHelper->getInstallmentValue($grandTotal, $installments, 'maxipago_payment_checkout2'));
        $info->setAdditionalInformation('installments', $installments);
        $info->setAdditionalInformation('base_grand_total', $grandTotal);

        return $this;
    }

    /**
     * @return $this|\Magento\Payment\Model\Method\Cc
     */
    public function validate()
    {
        return $this;
    }

    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return $this|\Magento\Payment\Model\Method\Cc
     * @throws LocalizedException
     */
    public function order(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        try {
            $code = $this->getCode();

            $order    = $payment->getOrder();
            $response = null;
            $amount   = number_format($amount, 2, '.', '');
            $hasInterest = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_checkout_2/installment_type');

            if ($hasInterest == 'N') {
                $amount = number_format($payment->getAdditionalInformation('total_with_interest'), 2, '.', '');
            }

            $operation = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_checkout_2/checkout2_payment_action');

            $orderId = $order->getIncrementId();

            $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();
            $currencysymbol = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
            $currencyCode   = $currencysymbol->getStore()->getCurrentCurrencyCode();

            $installments = $payment->getAdditionalInformation('installments');
            $description  = $payment->getAdditionalInformation('description');
            $comments     = $payment->getAdditionalInformation('comments');
            $subject      = $payment->getAdditionalInformation('subject');
            $cpfCnpj      = $payment->getAdditionalInformation('cpf_cnpj');

            $ipAddress  = $this->getIpAddress();
            $fraudCheck = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_checkout_2/fraud_check') ? 'Y' : 'N';

            $dayToExpire = (int) $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_checkout_2/due_days');
            $processorId = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_checkout_2/processor_id');

            $date = new \DateTime();
            $date->modify('+' . $dayToExpire . ' days');

            $expirationDate = $date->format('Y-m-d');
            $expirationDate = $this->_maxipagoHelper->getDate($expirationDate, 'm/d/Y');

            $amount = number_format($amount, 2, '.', '');

            $data = array(
                'referenceNum'         => $orderId,
                'processorID'          => $processorId,
                'customerIdExt'        => $this->clearCpfCnpj($cpfCnpj),
                'fraudCheck'           => $fraudCheck,
                'ipAddress'            => $ipAddress,
                'currencyCode'         => 'BRL',
                'numberOfInstallments' => $installments,
                'expirationDate'       => $expirationDate,
                'chargeTotal'          => $amount,
                'description'          => $description,
                'comments'             => $comments,
                'emailSubject'         => $subject,
                'operation'            => $operation
            );

            $customerData = $this->_maxipagoCustomerHelper->getCustomerData($order->getCustomerId(), 'm/d/Y');
            $data = array_merge($data, $customerData);

            $this->helperApi->authenticate();

            $this->_libMaxiPago->addPaymentOrder($data);
            $response = $this->_libMaxiPago->response;

            $this->_maxipagoHelper->debug($this->_libMaxiPago);

            if (
                (isset($response['errorMessage']) && $response['errorMessage'])
                ||
                (isset($response['errorMsg']) && $response['errorMsg'])
            ) {
                $error = isset($response['errorMessage']) ? $response['errorMessage'] : $response['errorMsg'];
                $this->_logger->critical($error);
            }

            if (isset($response['errorCode']) && $response['errorCode'] != 1) {
                $payment = $this->setAdditionalInfo($payment, $response);
            } else {
                if ($this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_checkout_2/stop_processing')) {
                    $errors = __('The transaction wasn\'t authorized by the issuer, please check your data and try again');
                    $this->_logger->critical($errors);
                }

                $payment->setSkipOrderProcessing(true);
                $this->_logger->critical(__('There was an error processing your request. Please contact us or try again later.'));
            }
        } catch (\Exception $e) {
            throw new LocalizedException(__('Payment failed ' . $e->getMessage()));
        }

        return $this;
    }

    /**
     * @param \Magento\Quote\Api\Data\CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        if (!$this->isActive($quote ? $quote->getStoreId() : null)) {
            return false;
        }

        return true;
    }

    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param $response
     * @return \Magento\Payment\Model\InfoInterface
     */
    protected function setAdditionalInfo(\Magento\Payment\Model\InfoInterface $payment, $response)
    {
        if (isset($response['transactionID'])) {
            $tid = $response['transactionID'];
            $payment->setAdditionalInformation('transaction_id', $tid);
            $payment->setTransactionId($tid);
        }

        if (isset($response['processorTransactionID'])) {
            $payment->setAdditionalInformation('processor_transaction_id', $response['processorTransactionID']);
        }

        if (isset($response['processorReferenceNumber'])) {
            $payment->setAdditionalInformation('processor_reference_number', $response['processorReferenceNumber']);
        }

        if (isset($response['creditCardScheme'])) {
            $payment->setAdditionalInformation('credit_card_scheme', $response['creditCardScheme']);
        }

        if (isset($response['creditCardCountry'])) {
            $payment->setAdditionalInformation('credit_card_scheme', $response['creditCardCountry']);
        }

        if (isset($response['responseMessage'])) {
            $payment->setAdditionalInformation('response_message', $response['responseMessage']);
        }

        if (isset($response['responseCode'])) {
            $payment->setAdditionalInformation('response_code', $response['responseCode']);
        }

        if (isset($response['authCode'])) {
            $payment->setAdditionalInformation('auth_code', $response['authCode']);
        }

        if (isset($response['orderID'])) {
            $payment->setAdditionalInformation('order_id', $response['orderID']);
        }

        if (isset($response['fraudScore'])) {
            $payment->setAdditionalInformation('fraud_score', $response['fraudScore']);
        }

        if (isset($response['transactionState'])) {
            $payment->setAdditionalInformation('last_transaction_state', $response['transactionState']);
        }

        if (isset($response['authenticationURL'])) {
            $payment->setAdditionalInformation('authentication_url', $response['authenticationURL']);
        }

        if (isset($response['boletoUrl'])) {
            $payment->setAdditionalInformation('boleto_url', $response['boletoUrl']);
        }

        if (isset($response['onlineDebitUrl'])) {
            $payment->setAdditionalInformation('boleto_url', $response['onlineDebitUrl']);
        }

        if (isset($response['creditCardCountry'])) {
            $payment->setAdditionalInformation('cc_country', $response['creditCardCountry']);
        }

        if (isset($response['creditCardScheme'])) {
            $payment->setAdditionalInformation('cc_scheme', $response['creditCardScheme']);
        }

        if (isset($response['result']) && isset($response['result']['pay_order_id'])) {
            $payment->setAdditionalInformation('pay_order_id', $response['result']['pay_order_id']);
        }

        return $payment;
    }

    /**
     * @param $value
     * @return mixed|string
     */
    private function clearCpfCnpj($value)
    {
        $value = trim($value);
        $value = str_replace(".", "", $value);
        $value = str_replace(",", "", $value);
        $value = str_replace("-", "", $value);
        $value = str_replace("/", "", $value);

        return $value;
    }

    /**
     * @param $ipAddress
     * @return string
     */
    private function validateIP($ipAddress)
    {
        if (filter_var($ipAddress, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            return $ipAddress;
        }

        return self::DEFAULT_IP;
    }

    /**
     * @return string
     */
    private function getIpAddress()
    {
        $om  = \Magento\Framework\App\ObjectManager::getInstance();
        $obj = $om->get('Magento\Framework\HTTP\PhpEnvironment\RemoteAddress');
        $ip  = $obj->getRemoteAddress();

        $ipAddress = $ip ? $this->validateIP($ip) : self::DEFAULT_IP;

        return $ipAddress;
    }
}