<?php

namespace MaxiPago\Payment\Model;

use Magento\Framework\UrlInterface;
use \Magento\Payment\Model\Method\AbstractMethod;
use Magento\Sales\Model\Order;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Sales\Model\Order\Payment;

class PaymentMethodDc extends \Magento\Payment\Model\Method\Cc
{
    /** @var int  */
    const ROUND_UP = 100;

    /** @var string  */
    const DEFAULT_IP = '127.0.0.1';

    /** @var int  */
    const DEFAULT_TICKET_BANK = 12;

    /** @var int  */
    const DEFAULT_EFT_BANK = 17;

    /** @var int  */
    const DEFAULT_REDEPAY_BANK = 18;

    /**
     * @var bool
     */
    protected $_canAuthorize = true;

    /**
     * @var bool
     */
    protected $_canCapture = true;

    /**
     * @var bool
     */
    protected $_canRefund = true;

    /**
     * @var string
     */
    protected $_code = 'maxipago_payment_dc';

    /**
     * @var bool
     */
    protected $_isGateway = true;

    /**
     * @var bool
     */
    protected $_canCapturePartial = true;

    /**
     * @var bool
     */
    protected $_canRefundInvoicePartial = true;

    /**
     * @var bool
     */
    protected $_canVoid = true;

    /**
     * @var bool
     */
    protected $_canCancel = true;

    /**
     * @var bool
     */
    protected $_canUseForMultishipping = false;

    /**
     * @var bool
     */
    protected $_canUseInternal = false;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $_countryFactory;

    /**
     * @var array
     */
    protected $_supportedCurrencyCodes = ['BRL'];

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    /**
     * @var \MaxiPago\Payment\Helper\Data
     */
    protected $_maxipagoHelper;

    /**
     * @var \MaxiPago\Payment\Helper\Customer
     */
    protected $_maxipagoCustomerHelper;

    /**
     * @var \MaxiPago\Payment\Helper\Order
     */
    protected $_maxipagoOrderHelper;

    /**
     * @var string
     */
    protected $_infoBlockType = 'MaxiPago\Payment\Block\Info\Ticket';

    /**
     * @var \MaxiPago\Payment\lib\MaxiPago
     */
    protected $_libMaxiPago;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * PaymentMethodDc constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param \Magento\Framework\Module\ModuleListInterface $moduleList
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \MaxiPago\Payment\Helper\Data $maxipagoHelper
     * @param \MaxiPago\Payment\Helper\Customer $maxipagoCustomerHelper
     * @param \MaxiPago\Payment\Helper\Order $maxipagoOrderHelper
     * @param \MaxiPago\Payment\lib\MaxiPago $libMaxiPago
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Checkout\Model\Cart $cart,
        \MaxiPago\Payment\Helper\Data $maxipagoHelper,
        \MaxiPago\Payment\Helper\Customer $maxipagoCustomerHelper,
        \MaxiPago\Payment\Helper\Order $maxipagoOrderHelper,
        \MaxiPago\Payment\lib\MaxiPago $libMaxiPago,
        array $data = []
    )
    {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $moduleList,
            $localeDate,
            null,
            null,
            $data
        );

        $this->_countryFactory         = $countryFactory;
        $this->scopeConfig             = $scopeConfig;
        $this->_cart                   = $cart;
        $this->_maxipagoHelper         = $maxipagoHelper;
        $this->_maxipagoCustomerHelper = $maxipagoCustomerHelper;
        $this->_maxipagoOrderHelper    = $maxipagoOrderHelper;
        $this->_libMaxiPago            = $libMaxiPago;
    }

    /**
     * @param \Magento\Framework\DataObject $data
     * @return $this|\Magento\Payment\Model\Method\Cc
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function assignData(\Magento\Framework\DataObject $data)
    {
        parent::assignData($data);

        $infoInstance = $this->getInfoInstance();
        $currentData  = $data->getAdditionalData();

        $cpfCnpj = isset($currentData['cpf_cnpj']) ? $currentData['cpf_cnpj'] : null;

        /*
        if (!$this->scopeConfig->getValue('payment/maxipago_payment/show_taxvat_field')) {
            $cpfCnpj = $this->_maxipagoHelper->getTaxvatValue();
        }
        */

        $dcType     = isset($currentData['dc_type']) ? $currentData['dc_type'] : null;
        $dcOwner    = isset($currentData['dc_owner']) ? $currentData['dc_owner'] : null;
        $dcNumber   = preg_replace("/[^0-9]/", '', isset($currentData['dc_number']) ? $currentData['dc_number'] : null);
        $dcCid      = isset($currentData['dc_cid']) ? $currentData['dc_cid'] : null;
        $dcExpMonth = str_pad(isset($currentData['month']) ? $currentData['month'] : null, 2, '0', STR_PAD_LEFT);
        $dcExpYear  = isset($currentData['year']) ? $currentData['year'] : null;

        $dcNumberEnc = $infoInstance->encrypt($dcNumber);
        $dcLast4     = substr($dcNumber, -4);
        $dcCidEnc    = $infoInstance->encrypt($dcCid);


        $infoInstance->setCcType($dcType);
        $infoInstance->setCcOwner($dcOwner);
        $infoInstance->setCcNumber($dcNumber);
        $infoInstance->setCcExpMonth($dcExpMonth);
        $infoInstance->setCcExpYear($dcExpYear);
        $infoInstance->setCcNumberEnc($dcNumberEnc);
        $infoInstance->setCcCidEnc($dcCidEnc);
        $infoInstance->setCcLast4($dcLast4);

        $infoInstance->setAdditionalInformation('cpf_cnpj', $cpfCnpj);

        $this->_registry->unregister('maxipago_dc_cid');
        $this->_registry->register('maxipago_dc_cid', $dcCid);

        return $this;
    }

    /**
     * @return $this|\Magento\Payment\Model\Method\Cc
     */
    public function validate()
    {
        return $this;
    }

    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return null
     * @throws LocalizedException
     */
    public function order(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        try {
            $order    = $payment->getOrder();
            $response = null;

            $orderId = $order->getIncrementId();

            $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();
            $currencysymbol = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
            $currencyCode   = $currencysymbol->getStore()->getCurrentCurrencyCode();

            $ipAddress = $this->getIpAddress();

            $dcCid = $payment->getCcCid();

            $dcOwner     = $payment->getCcOwner();
            $dcNumber    = $payment->decrypt($payment->getCcNumberEnc());
            $dcExpMonth  = str_pad($payment->getCcExpMonth(), 2, '0', STR_PAD_LEFT);
            $dcExpYear   = $payment->getCcExpYear();
            $dcBrand     = $payment->getCcType();
            $processorID = $this->_maxipagoHelper->getProcessor($dcBrand, $this->_code);
            $amount      = number_format($amount, 2, '.', '');

            $data = array(
                'referenceNum' => $orderId,
                'processorID'  => $processorID,
                'ipAddress'    => $ipAddress,
                'number'       => $dcNumber,
                'expMonth'     => $dcExpMonth,
                'expYear'      => $dcExpYear,
                'cvvNumber'    => $dcCid,
                'currencyCode' => $currencyCode,
                'chargeTotal'  => $amount
            );

            $data['mpiProcessorID'] = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_dc/mpi_processor');
            $data['onFailure'] = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_dc/failure_action');

            $billingData  = $this->_maxipagoCustomerHelper->getAddressData($order->getCustomerId(), $payment);
            $shippingData = $this->_maxipagoCustomerHelper->getAddressData($order->getCustomerId(), $payment, 'shipping');

            $orderData = $this->_maxipagoOrderHelper->getOrderData($payment);

            $data = array_merge($data, $billingData, $shippingData, $orderData);

            $merchantId     = $this->scopeConfig->getValue('payment/maxipago_payment/id');
            $merchantKey    = $this->scopeConfig->getValue('payment/maxipago_payment/store_key');
            $merchantSecret = $this->scopeConfig->getValue('payment/maxipago_payment/secret_store_key');

            if ($merchantId && $merchantSecret) {
                $environment = $this->scopeConfig->getValue('payment/maxipago_payment/test_environment');
                $this->_libMaxiPago->setCredentials($merchantId, $merchantKey);
                $this->_libMaxiPago->setEnvironment($environment);
            }

            $this->_libMaxiPago->saledebitCard3DS($data);

            $response = $this->_libMaxiPago->response;

            $this->_maxipagoHelper->debug($this->_libMaxiPago);

            if (isset($response['transactionID']) && $response['transactionID']) {

                $tid = $response['transactionID'];
                $payment->setCcTransId($tid);

                $payment = $this->setAdditionalInfo($payment, $response);

                if ($response['responseCode'] != 0 && $response['responseCode'] != 5){

                    if ($this->scopeConfig->getValue('payment/maxipago_payment/stop_processing')) {
                        throw new \Exception('The transaction wasn\'t authorized by the issuer, please check your data and try again');
                    }

                    $payment->setSkipOrderProcessing(true);
                } else {

                    if (isset($response['authenticationURL'])) {
                        $this->_registry->register('maxipago_redirect_url', $response['authenticationURL']);
                    }

                }

            } else {
                throw new \Exception('There was an error processing your request. Please contact us or try again later.');
            }

        } catch (\Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }

        return $response;
    }

    /**
     * @param \Magento\Quote\Api\Data\CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        if (!$this->isActive($quote ? $quote->getStoreId() : null)) {
            return false;
        }
        return true;
    }

    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param $response
     * @return \Magento\Payment\Model\InfoInterface
     */
    protected function setAdditionalInfo(\Magento\Payment\Model\InfoInterface $payment, $response)
    {
        if (isset($response['transactionID'])) {
            $tid = $response['transactionID'];
            $payment->setAdditionalInformation('transaction_id', $tid);
            $payment->setTransactionId($tid);
        }

        if (isset($response['processorTransactionID'])) {
            $payment->setAdditionalInformation('processor_transaction_id', $response['processorTransactionID']);
        }

        if (isset($response['processorReferenceNumber'])) {
            $payment->setAdditionalInformation('processor_reference_number', $response['processorReferenceNumber']);
        }

        if (isset($response['creditCardScheme'])) {
            $payment->setAdditionalInformation('credit_card_scheme', $response['creditCardScheme']);
        }

        if (isset($response['creditCardCountry'])) {
            $payment->setAdditionalInformation('credit_card_scheme', $response['creditCardCountry']);
        }

        if (isset($response['responseMessage'])) {
            $payment->setAdditionalInformation('response_message', $response['responseMessage']);
        }

        if (isset($response['responseCode'])) {
            $payment->setAdditionalInformation('response_code', $response['responseCode']);
        }

        if (isset($response['authCode'])) {
            $payment->setAdditionalInformation('auth_code', $response['authCode']);
        }

        if (isset($response['orderID'])) {
            $payment->setAdditionalInformation('order_id', $response['orderID']);
        }

        if (isset($response['fraudScore'])) {
            $payment->setAdditionalInformation('fraud_score', $response['fraudScore']);
        }

        if (isset($response['transactionState'])) {
            $payment->setAdditionalInformation('last_transaction_state', $response['transactionState']);
        }

        if (isset($response['authenticationURL'])) {
            $payment->setAdditionalInformation('authentication_url', $response['authenticationURL']);
        }

        if (isset($response['boletoUrl'])) {
            $payment->setAdditionalInformation('boleto_url', $response['boletoUrl']);
        }

        if (isset($response['onlineDebitUrl'])) {
            $payment->setAdditionalInformation('boleto_url', $response['onlineDebitUrl']);
        }

        if (isset($response['creditCardCountry'])) {
            $payment->setAdditionalInformation('dc_country', $response['creditCardCountry']);
        }

        if (isset($response['creditCardScheme'])) {
            $payment->setAdditionalInformation('dc_scheme', $response['creditCardScheme']);
        }

        if (isset($response['result']) && isset($response['result']['pay_order_id'])) {
            $payment->setAdditionalInformation('pay_order_id', $response['result']['pay_order_id']);
        }

        return $payment;
    }

    /**
     * @param $ipAddress
     * @return string
     */
    private function validateIP($ipAddress)
    {
        if (filter_var($ipAddress, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            return $ipAddress;
        }

        return self::DEFAULT_IP;
    }

    /**
     * @return string
     */
    private function getIpAddress()
    {
        $om  = \Magento\Framework\App\ObjectManager::getInstance();
        $obj = $om->get('Magento\Framework\HTTP\PhpEnvironment\RemoteAddress');
        $ip  = $obj->getRemoteAddress();

        $ipAddress = $ip ? $this->validateIP($ip) : self::DEFAULT_IP;
        return $ipAddress;
    }
}