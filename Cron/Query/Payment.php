<?php

namespace se\Cron\Query;

class Payment
{
    CONST CRON_FILE = 'maxipago-cron.log';

    /**
     * @var \MaxiPago\Payment\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $_directorylist;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \MaxiPago\Payment\lib\MaxiPago
     */
    protected $libMaxiPago;

    /**
     * @var \MaxiPago\Payment\Helper\Api
     */
    protected $apiHelper;

    /**
     * @var \MaxiPago\Payment\Helper\Order
     */
    protected $orderHelper;

    /**
     * Payment constructor.
     * @param \MaxiPago\Payment\Logger\Logger $logger
     * @param \Magento\Framework\App\Filesystem\DirectoryList $_directorylist
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \MaxiPago\Payment\lib\MaxiPago $libMaxiPago
     * @param \MaxiPago\Payment\Helper\Api $apiHelper
     * @param \MaxiPago\Payment\Helper\Order $orderHelper
     */
    public function __construct(
        \MaxiPago\Payment\Logger\Logger $logger,
        \Magento\Framework\App\Filesystem\DirectoryList $_directorylist,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \MaxiPago\Payment\lib\MaxiPago $libMaxiPago,
        \MaxiPago\Payment\Helper\Api $apiHelper,
        \MaxiPago\Payment\Helper\Order $orderHelper
    )
    {
        $this->logger = $logger;
        $this->_directorylist = $_directorylist;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->scopeConfig = $scopeConfig;
        $this->libMaxiPago = $libMaxiPago;
        $this->apiHelper = $apiHelper;
        $this->orderHelper = $orderHelper;
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $this->logger->info('STARTING CRON');

        $ninStatuses = [
            'complete',
            'canceled',
            'closed',
            'holded'
        ];

        $date = new \DateTime('-15 DAYS');
        $fromDate = $date->format('Y-m-d');

        $orderCollection = $this->orderCollectionFactory->create()->addFieldToSelect(array('*'));

        $orderCollection->getSelect()
            ->join(array('payment' => 'sales_order_payment'), 'main_table.entity_id=payment.parent_id',
                array('payment_method' => 'payment.method',
                    'order_id' => 'main_table.entity_id'
                )
            );

        $orderCollection
            ->addFieldToFilter('payment.method', ['like' => 'maxipago_%'])
            ->addFieldToFilter('state', array('nin' => array($ninStatuses)))
            ->addFieldToFilter('created_at', array('gt' => $fromDate));


        foreach ($orderCollection as $order) {
            if ($order->getId()) {

                $this->logger->info('pullReport ' . $order->getIncrementId());
                
                $response = $this->apiHelper->pullReport($order, false, self::CRON_FILE);

                if (isset($response['records'])) {

                    $record = isset($response['records'][0]) ? $response['records'][0] : null;

                    if ($record) {
                        $this->orderHelper->updatePayment($order, $record);
                    }

                }

            }
        }

        $this->logger->info('ENDING CRON');
    }
}