<?php

namespace MaxiPago\Payment\Helper;

class Card extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \MaxiPago\Payment\Logger\Logger
     */
    protected $maxipagoLogger;

    /**
     * @var \MaxiPago\Payment\lib\MaxiPago
     */
    protected $libMaxiPago;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var Customer 
     */
    protected $customerHelper;

    /**
     * @var \MaxiPago\Payment\Model\CardFactory
     */
    protected $cardFactory;

    /**
     * @var Api
     */
    protected $helperApi;

    /**
     * @var \MaxiPago\Payment\Helper\Data
     */
    protected $_maxipagoHelper;

    /**
     * Card constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \MaxiPago\Payment\Logger\Logger $maxipagoLogger
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \MaxiPago\Payment\lib\MaxiPago $libMaxiPago
     * @param \Psr\Log\LoggerInterface $logger
     * @param Customer $customerHelper
     * @param \MaxiPago\Payment\Model\CardFactory $cardFactory
     * @param Api $helperApi
     * @param Data $maxipagoHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \MaxiPago\Payment\Logger\Logger $maxipagoLogger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \MaxiPago\Payment\lib\MaxiPago $libMaxiPago,
        \Psr\Log\LoggerInterface $logger,
        \MaxiPago\Payment\Helper\Customer $customerHelper,
        \MaxiPago\Payment\Model\CardFactory $cardFactory,
        \MaxiPago\Payment\Helper\Api $helperApi,
        \MaxiPago\Payment\Helper\Data $maxipagoHelper
    )
    {
        parent::__construct($context);

        $this->maxipagoLogger = $maxipagoLogger;
        $this->scopeConfig = $scopeConfig;
        $this->libMaxiPago = $libMaxiPago;
        $this->logger = $logger;
        $this->customerHelper = $customerHelper;
        $this->cardFactory = $cardFactory;
        $this->helperApi = $helperApi;
        $this->_maxipagoHelper = $maxipagoHelper;
    }

    /**
     * @param $payment
     * @return bool
     */
    public function saveCard($payment)
    {
        try {
            $order   = $payment->getOrder();
            $address = $this->customerHelper->getAddressData($order->getCustomerId(), $payment);

            $customerId = $order->getCustomerId();
            $firstname  = $order->getCustomerFirstname();
            $lastname   = $order->getCustomerLastname();
            $mpCustomerId = null;

            $ccBrand    = $payment->getCcType();
            $ccNumber   = $payment->decrypt($payment->getCcNumberEnc());
            $ccExpMonth = str_pad($payment->getCcExpMonth(), 2, '0', STR_PAD_LEFT);
            $ccExpYear  = $payment->getCcExpYear();

            $mpCustomer = $this->customerHelper->getMaxipagoCustomer($order);

            if ($mpCustomer->getId()) {

                $description = substr($ccNumber, 0, 6) . 'XXXXXX' . substr($ccNumber, -4, 4);

                $cards = $this->cardFactory->create()->getCollection()
                    ->addFieldToFilter('description', $description)
                    ->addFieldToFilter('customer_id_maxipago', $mpCustomer->getCustomerIdMaxipago())
                ;

                if (!$cards->count()) {

                    $date = new \DateTime($ccExpYear . '-' . $ccExpMonth . '-01');
                    $date->modify('+1 month');
                    $endDate = $date->format('m/d/Y');
                    $payment->getAdditionalInformation('cpf_cnpj');
                    $mpCustomerId = $mpCustomer->getCustomerIdMaxipago();

                    $ccData = array(
                        'customerId'           => $mpCustomerId,
                        'creditCardNumber'     => $ccNumber,
                        'expirationMonth'      => $ccExpMonth,
                        'expirationYear'       => $ccExpYear,
                        'billingName'          => $firstname . ' ' . $lastname,
                        'billingAddress1'      => $address['billingAddress'],
                        'billingAddress2'      => $address['billingAddress2'],
                        'billingCity'          => $address['billingCity'],
                        'billingState'         => $address['billingState'],
                        'billingZip'           => $address['billingZip'],
                        'billingPostCode'      => $address['billingZip'],
                        'billingCountryCode'   => $address['billingCountryCode'],
                        'billingPhoneType'     => $address['billingPhoneType'],
                        'billingPhoneAreaCode' => $address['billingPhoneAreaCode'],
                        'billingPhoneNumber'   => $address['billingPhoneNumber'],
                        'billingEmail'         => $address['billingEmail'],
                        'onFileEndDate'        => $endDate,
                        'onFilePermissions'    => 'ongoing'
                    );

                    $this->helperApi->authenticate();

                    $this->libMaxiPago->addCreditCard($ccData);
                    
                    $token = $this->libMaxiPago->getToken();
                    $response = $this->libMaxiPago->response;

                    $this->_maxipagoHelper->debug($this->libMaxiPago);

                    if ($token) {
                        $card = $this->cardFactory->create();

                        $card->setData(
                            array(
                                'customer_id'          => $customerId,
                                'customer_id_maxipago' => $mpCustomerId,
                                'token'                => $token,
                                'description'          => $description,
                                'brand'                => $ccBrand
                            )
                        );
                        
                        $card->save();
                    }
                }
            }
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @param $customerId
     * @return mixed
     */
    public function getSavedCards($customerId)
    {
        $collection = $this->cardFactory->create()->getCollection()
            ->addFieldToFilter('customer_id', $customerId);

        return $collection;
    }

    /**
     * @param $entityId
     * @return \MaxiPago\Payment\Model\Card
     */
    public function getSavedCard($entityId)
    {
        return $this->cardFactory->create()->load($entityId);
    }
}