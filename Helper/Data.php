<?php

namespace MaxiPago\Payment\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\App\Action;
use MaxiPago\MaxiPago;
use MaxiPago\Auth\OAuth;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    CONST MAXIPAGO_FILE = 'maxipago.log';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var
     */
    protected $tokenauth;

    /**
     * @var
     */
    protected $keyauth;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var array
     */
    protected $_availableMethods = array(
        'maxipago_payment_cc',
        'maxipago_payment_dc',
        'maxipago_payment_ticket',
        'maxipago_payment_tef',
        'maxipago_payment_checkout2'
    );

    /**
     * @var array
     */
    protected $_brands = array(
        'VI' => 'Visa',
        'MC' => 'Mastercard',
        'AM' => 'Amex',
        'DC' => 'Diners Club',
        'EL' => 'Elo',
        'DI' => 'Discover',
        'HC' => 'Hipercard',
        'HI' => 'Hiper',
        'AU' => 'Aura',
        'JC' => 'JCB',
        'CR' => 'Credz'
    );

    /**
     * @var array
     */
    protected $_banks = array(
        '11' => 'Itaú',
        '12' => 'Bradesco',
        '13' => 'Banco do Brasil',
        '14' => 'HSBC',
        '15' => 'Santander',
        '16' => 'Caixa Econômica Federal',
        '17' => 'Bradesco',
        '18' => 'Itaú',
    );

    /**
     * @var array
     */
    protected $_responseCodes = array(
        '0' => 'Pagamento Aprovado',
        '1' => 'Pagamento Reprovado',
        '2' => 'Pagamento Reprovado',
        '5' => 'Pagamento em análise',
        '1022' => 'Ocorreu um erro com a finalizadora, entre em contato com nossa equipe',
        '1024' => 'Erros, dados enviados inválidos, entre em contato com nossa equipe',
        '1025' => 'Erro nas credenciais de envio, entre em contato com nossa equipe',
        '2048' => 'Erro interno, entre em contato com nossa equipe',
        '4097' => 'Erro de tempo de execução, entre em contato com nossa equipe'
    );

    /**
     * @var array
     */
    protected $_transactionStates = array(
        'in_progress' => array('code' => '1', 'label' => 'Em Progresso'),
        'captured' => array('code' => '3', 'label' => 'Capturado'),
        'pending_capture' => array('code' => '4', 'label' => 'Pendente de Captura'),
        'pending_authorization' => array('code' => '5', 'label' => 'Pendente de Autorização'),
        'authorized' => array('code' => '6', 'label' => 'Autorizado'),
        'declined' => array('code' => '7', 'label' => 'Recuzado'),
        'reversed' => array('code' => '8', 'label' => 'Recusado'),
        'voided' => array('code' => '9', 'label' => 'Anulado'),
        'paid' => array('code' => '10', 'label' => 'Pago'),
        'pending_confirmation' => array('code' => '11', 'label' => 'Pendente de Confirmação'),
        'pending_review' => array('code' => '12', 'label' => 'Pendente de Revisão (verificar com o suporte)'),
        'pending_reversion' => array('code' => '13', 'label' => 'Pendente de Revisão'),
        'pending_capture_retrial' => array('code' => '14', 'label' => 'Pendente de Captura (re-tentativa)'),
        'pending_reversal_retrial' => array('code' => '16', 'label' => 'Pendente de Estorno'),
        'pending_void' => array('code' => '18', 'label' => 'Pendente Anulação'),
        'pending_void_retrial' => array('code' => '19', 'label' => 'Pendente Anulação (re-tentativa)'),
        'boleto_issued' => array('code' => '22', 'label' => 'Boleto Emitido'),
        'pending_authentication' => array('code' => '29', 'label' => 'Pendente Autenticação'),
        'authenticated' => array('code' => '30', 'label' => 'Autenticado'),
        'pending_reversal' => array('code' => '31', 'label' => 'Pendente Estorno (retentativa)'),
        'authentication_in_progress' => array('code' => '32', 'label' => 'Autenticação em Progresso'),
        'submitted_authentication' => array('code' => '33', 'label' => 'Autenticação Enviada'),
        'boleto_viewed' => array('code' => '34', 'label' => 'Boleto Visualizado'),
        'boleto_underpaid' => array('code' => '35', 'label' => 'Boleto Pago com valor abaixo'),
        'boleto_overpaid' => array('code' => '36', 'label' => 'Boleto Pago com valor acima'),
        'file_submission_pending_reversal' => array('code' => '38', 'label' => 'File submission Pendente Reversal'),
        'fraud_approved' => array('code' => '44', 'label' => 'Análise de Fraude Aprovada'),
        'fraud_declined' => array('code' => '45', 'label' => 'Análise de Fraude Recusada'),
        'fraud_review' => array('code' => '46', 'label' => 'Análise de Fraude Revisão')
    );

    /**
     * @var array
     */
    protected $_countryCodes = [
        'AD' => '376',
        'AE' => '971',
        'AF' => '93',
        'AG' => '1268',
        'AI' => '1264',
        'AL' => '355',
        'AM' => '374',
        'AN' => '599',
        'AO' => '244',
        'AQ' => '672',
        'AR' => '54',
        'AS' => '1684',
        'AT' => '43',
        'AU' => '61',
        'AW' => '297',
        'AZ' => '994',
        'BA' => '387',
        'BB' => '1246',
        'BD' => '880',
        'BE' => '32',
        'BF' => '226',
        'BG' => '359',
        'BH' => '973',
        'BI' => '257',
        'BJ' => '229',
        'BL' => '590',
        'BM' => '1441',
        'BN' => '673',
        'BO' => '591',
        'BR' => '55',
        'BS' => '1242',
        'BT' => '975',
        'BW' => '267',
        'BY' => '375',
        'BZ' => '501',
        'CA' => '1',
        'CC' => '61',
        'CD' => '243',
        'CF' => '236',
        'CG' => '242',
        'CH' => '41',
        'CI' => '225',
        'CK' => '682',
        'CL' => '56',
        'CM' => '237',
        'CN' => '86',
        'CO' => '57',
        'CR' => '506',
        'CU' => '53',
        'CV' => '238',
        'CX' => '61',
        'CY' => '357',
        'CZ' => '420',
        'DE' => '49',
        'DJ' => '253',
        'DK' => '45',
        'DM' => '1767',
        'DO' => '1809',
        'DZ' => '213',
        'EC' => '593',
        'EE' => '372',
        'EG' => '20',
        'ER' => '291',
        'ES' => '34',
        'ET' => '251',
        'FI' => '358',
        'FJ' => '679',
        'FK' => '500',
        'FM' => '691',
        'FO' => '298',
        'FR' => '33',
        'GA' => '241',
        'GB' => '44',
        'GD' => '1473',
        'GE' => '995',
        'GH' => '233',
        'GI' => '350',
        'GL' => '299',
        'GM' => '220',
        'GN' => '224',
        'GQ' => '240',
        'GR' => '30',
        'GT' => '502',
        'GU' => '1671',
        'GW' => '245',
        'GY' => '592',
        'HK' => '852',
        'HN' => '504',
        'HR' => '385',
        'HT' => '509',
        'HU' => '36',
        'ID' => '62',
        'IE' => '353',
        'IL' => '972',
        'IM' => '44',
        'IN' => '91',
        'IQ' => '964',
        'IR' => '98',
        'IS' => '354',
        'IT' => '39',
        'JM' => '1876',
        'JO' => '962',
        'JP' => '81',
        'KE' => '254',
        'KG' => '996',
        'KH' => '855',
        'KI' => '686',
        'KM' => '269',
        'KN' => '1869',
        'KP' => '850',
        'KR' => '82',
        'KW' => '965',
        'KY' => '1345',
        'KZ' => '7',
        'LA' => '856',
        'LB' => '961',
        'LC' => '1758',
        'LI' => '423',
        'LK' => '94',
        'LR' => '231',
        'LS' => '266',
        'LT' => '370',
        'LU' => '352',
        'LV' => '371',
        'LY' => '218',
        'MA' => '212',
        'MC' => '377',
        'MD' => '373',
        'ME' => '382',
        'MF' => '1599',
        'MG' => '261',
        'MH' => '692',
        'MK' => '389',
        'ML' => '223',
        'MM' => '95',
        'MN' => '976',
        'MO' => '853',
        'MP' => '1670',
        'MR' => '222',
        'MS' => '1664',
        'MT' => '356',
        'MU' => '230',
        'MV' => '960',
        'MW' => '265',
        'MX' => '52',
        'MY' => '60',
        'MZ' => '258',
        'NA' => '264',
        'NC' => '687',
        'NE' => '227',
        'NG' => '234',
        'NI' => '505',
        'NL' => '31',
        'NO' => '47',
        'NP' => '977',
        'NR' => '674',
        'NU' => '683',
        'NZ' => '64',
        'OM' => '968',
        'PA' => '507',
        'PE' => '51',
        'PF' => '689',
        'PG' => '675',
        'PH' => '63',
        'PK' => '92',
        'PL' => '48',
        'PM' => '508',
        'PN' => '870',
        'PR' => '1',
        'PT' => '351',
        'PW' => '680',
        'PY' => '595',
        'QA' => '974',
        'RO' => '40',
        'RS' => '381',
        'RU' => '7',
        'RW' => '250',
        'SA' => '966',
        'SB' => '677',
        'SC' => '248',
        'SD' => '249',
        'SE' => '46',
        'SG' => '65',
        'SH' => '290',
        'SI' => '386',
        'SK' => '421',
        'SL' => '232',
        'SM' => '378',
        'SN' => '221',
        'SO' => '252',
        'SR' => '597',
        'ST' => '239',
        'SV' => '503',
        'SY' => '963',
        'SZ' => '268',
        'TC' => '1649',
        'TD' => '235',
        'TG' => '228',
        'TH' => '66',
        'TJ' => '992',
        'TK' => '690',
        'TL' => '670',
        'TM' => '993',
        'TN' => '216',
        'TO' => '676',
        'TR' => '90',
        'TT' => '1868',
        'TV' => '688',
        'TW' => '886',
        'TZ' => '255',
        'UA' => '380',
        'UG' => '256',
        'US' => '1',
        'UY' => '598',
        'UZ' => '998',
        'VA' => '39',
        'VC' => '1784',
        'VE' => '58',
        'VG' => '1284',
        'VI' => '1340',
        'VN' => '84',
        'VU' => '678',
        'WF' => '681',
        'WS' => '685',
        'XK' => '381',
        'YE' => '967',
        'YT' => '262',
        'ZA' => '27',
        'ZM' => '260',
        'ZW' => '263'
    ];

    const ROUND_UP = 100;

    const REDIRECT_URI_SANDBOX = "http://loja.maxipago.com.br/magento2/redirect/";
    const URL_KEY_SANDBOX = "https://sandbox.maxipago.com.br/v2/keys/";
    const APP_ID_SANDBOX = "APP-CKN5214B60GC";
    const CLIENT_SECRECT_SANDBOX = "5be91ec716cb46b8844861237168c8dc";

    const REDIRECT_URI_PRODUCTION = "http://loja.maxipago.com.br/magento2/redirect/";
    const URL_KEY_PRODUCTION = "https://api.maxipago.com.br/v2/keys/";
    const APP_ID_PRODUCTION = "APP-ZDVW5HTDKG16";
    const CLIENT_SECRECT_PRODUCTION = "cb49330579e144fdb40e22b50e04269e";

    /**
     * @var \MaxiPago\Payment\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepositoryInterface;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Magento\Backend\Model\Session\Quote
     */
    protected $backendSessionQuote;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \MaxiPago\Payment\Logger\Logger $logger
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Magento\Framework\App\State $state
     * @param \Magento\Backend\Model\Session\Quote $backendSessionQuote
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \MaxiPago\Payment\Logger\Logger $logger,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Framework\App\State $state,
        \Magento\Backend\Model\Session\Quote $backendSessionQuote
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->_objectManager = $objectManager;
        $this->date = $date;
        $this->_storeManager = $storeManager;
        $this->logger = $logger;
        $this->checkoutSession = $checkoutSession;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->state = $state;
        $this->backendSessionQuote = $backendSessionQuote;
    }

    /**
     * @param $libMaxiPago
     */
    public function debug($libMaxiPago)
    {
        if ($this->scopeConfig->getValue('payment/maxipago_payment/debug')) {
            $xmlRequest = $this->formatLog($libMaxiPago->xmlRequest);
            $xmlRequest = $this->formatLog($xmlRequest, 'cvvNumber');

            $this->logger->info('============');
            $this->logger->info('============');
            $this->logger->info('============');
            $this->logger->info('REQUEST:');
            $this->logger->info($xmlRequest);
            $this->logger->info('RESPONSE:');
            $this->logger->info($libMaxiPago->xmlResponse);
            $this->logger->info('============');
            $this->logger->info('============');
            $this->logger->info('============');
        }
    }

    /**
     * @param $xml
     * @param string $tag
     * @return null|string|string[]
     */
    public function formatLog($xml, $tag = 'number')
    {
        $xml = preg_replace_callback(
            '/<' . $tag . '>(.*)<\/' . $tag . '>/m',
            function ($matches) use ($tag) {
                $number = $matches[1];
                if (isset($matches[1]) && !empty($matches[1])) {
                    if (strlen($number) > 10) {
                        $number = substr($number, 0, 6) . 'XXXXXX' . substr($number, -4, 4);
                    } else {
                        $number = 'XXXX';
                    }
                    return '<' . $tag . '>' . $number . '</' . $tag . '>';
                }
                return false;
            },
            $xml
        );

        return $xml;
    }

    /**
     * @return array
     */
    public function getAvailableMethods()
    {
        return $this->_availableMethods;
    }

    /**
     * @return mixed
     */
    public function getTypeForCNPJ()
    {
        return $this->scopeConfig->getValue('payment/maxipagobase/advanced/type_cnpj', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getTypeForCpf()
    {
        return $this->scopeConfig->getValue('payment/maxipagobase/advanced/type_cpf', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getTypeNameCompany()
    {
        return $this->scopeConfig->getValue('payment/maxipagobase/advanced/type_name_company', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCpfAttributeForCustomer()
    {
        return $this->scopeConfig->getValue('payment/maxipagobase/advanced/cpf_for_customer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCpfAttributeForAddress()
    {
        return $this->scopeConfig->getValue('payment/maxipagobase/advanced/cpf_for_address', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCNPJAttributeForCustomer()
    {
        return $this->scopeConfig->getValue('payment/maxipagobase/advanced/cnpj_for_customer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCNPJAttributeForAddress()
    {
        return $this->scopeConfig->getValue('payment/maxipagobase/advanced/cnpj_for_address', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCompanyAttributeForAddress()
    {
        return $this->scopeConfig->getValue('payment/maxipagobase/advanced/company_name_address', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCompanyAttributeForCustomer()
    {
        $attribute_cpf = $this->scopeConfig->getValue('payment/maxipagobase/advanced/company_name_customer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $attribute_cpf;
    }

    /**
     * @return mixed
     */
    public function getStreetPositionLogradouro()
    {
        $street_logradouro = $this->scopeConfig->getValue('payment/maxipagobase/advanced/street_logradouro', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $street_logradouro;
    }

    /**
     * @return mixed
     */
    public function getStreetPositionNumber()
    {
        $street_logradouro = $this->scopeConfig->getValue('payment/maxipagobase/advanced/street_number', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $street_logradouro;
    }

    /**
     * @return mixed
     */
    public function getStreetPositionComplemento()
    {
        $street_logradouro = $this->scopeConfig->getValue('payment/maxipagobase/advanced/street_complemento', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $street_logradouro;
    }

    /**
     * @return mixed
     */
    public function getStreetPositionDistrict()
    {
        $street_logradouro = $this->scopeConfig->getValue('payment/maxipagobase/advanced/street_district', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $street_logradouro;
    }

    /**
     * @param $line
     * @return mixed
     */
    public function getInstructionLines($line)
    {
        return $this->scopeConfig->getValue('payment/maxipago_payment_ticket/instrucao' . $line, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getTypeInterest()
    {
        return $this->scopeConfig->getValue('payment/maxipago_payment_cc/installment/type_interest', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param $installment
     * @return mixed
     */
    public function getRate($installment)
    {
        return $this->scopeConfig->getValue('payment/maxipago_payment_cc/installment/installment_' . $installment, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getDueNumber()
    {
        return $this->scopeConfig->getValue('payment/maxipago_payment_ticket/expiration', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getImgForTicket()
    {
        return $this->scopeConfig->getValue('payment/maxipago_payment_ticket/logo_ticket', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param $environment
     * @return mixed
     */
    public function getOauth($environment)
    {
        return $this->scopeConfig->getValue('payment/maxipagobase/oauth_' . $environment, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getEnvironmentMode()
    {
        return $this->scopeConfig->getValue('payment/maxipagobase/environment_mode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param $type
     * @return mixed
     */
    public function getInfoUrlPreferenceInfo($type)
    {
        $_environment = $this->getEnvironmentMode();

        $id = $this->scopeConfig->getValue(
            'payment/maxipagobase/' . $type . '_id_' . $_environment, \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        return $id;
    }

    /**
     * @param $type
     * @return mixed
     */
    public function getInfoUrlPreferenceToken($type)
    {
        $_environment = $this->getEnvironmentMode();
        $token = $this->scopeConfig->getValue(
            'payment/maxipagobase/' . $type . '_token_' . $_environment, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $token;
    }

    /**
     * @param string $country
     * @return mixed|null
     */
    public function getCountryCode($country = 'BR')
    {
        return isset($this->_countryCodes[$country]) ? $this->_countryCodes[$country] : null;
    }

    /**
     * @param $telefone
     * @return bool|mixed|string
     */
    public function getPhoneNumber($telefone)
    {
        if (strlen($telefone) >= 10) {
            $telefone = preg_replace('/^D/', '', $telefone);
            $telefone = substr($telefone, 2, strlen($telefone) - 2);

        }
        return $telefone;
    }

    /**
     * @param $telefone
     * @return bool|mixed|string
     */
    public function getAreaNumber($telefone)
    {
        $telefone = preg_replace('/^D/', '', $telefone);
        $telefone = substr($telefone, 0, 2);
        return $telefone;
    }

    /**
     * @param $dob
     * @param string $format
     * @return string
     */
    public function getDate($dob, $format = 'Y-m-d')
    {
        $date = new \DateTime($dob);
        return $date->format($format);
    }

    /**
     * @param $config_path
     * @return mixed
     */
    public function getConfig($configPath)
    {
        return $this->scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param string $code
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getInstallmentsInformation($code = 'maxipago_payment_cc')
    {
        $quote = $this->getSession()->getQuote();

        $installmentsInformation = array();
        $paymentAmount = $quote->getBaseGrandTotal();

        $installments = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/max_installments');
        $installmentsWithoutInterest = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/installments_without_interest_rate');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $priceHelper = $objectManager->create('Magento\Framework\Pricing\Helper\Data');

        for ($i = 1; $i <= $installments; $i++) {
            if (($installments > $installmentsWithoutInterest) && ($i > $installmentsWithoutInterest)) {
                $interestRate = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/interest_rate');
                $value = $this->getInstallmentValue($paymentAmount, $i);
            } else {
                $interestRate = 0;
                $value = $paymentAmount / $i;
            }

            if ($value < $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/minimum_installments_value') && $i > 1) {
                continue;
            }

            $installmentsValue = null;
            $total = $value * $i;

            if ($interestRate) {
                $installmentsValue = $i . 'x de ' . $priceHelper->currency($value, true, false) . ' (Total de ' . $priceHelper->currency($total, true, false) . ' juros de ' . $interestRate . ' a.m.)';
            } else {
                $installmentsValue = $i . 'x de ' . $priceHelper->currency($value, true, false) . ' (sem juros)';
            }

            $installmentsInformation[] = array(
                'total' => $total,
                'value' => $i,
                'installments' => $installmentsValue,
                'interest_rate' => $interestRate
            );
        }

        return $installmentsInformation;
    }

    /**
     * @param $brand
     * @param string $code
     * @return int
     */
    public function getProcessor($brand, $code = 'maxipago_payment_cc')
    {
        $multiprocessors = $this->scopeConfig->getValue('payment/maxipago_payment/' . $code . '/processors');

        if ($multiprocessors) {
            $processors = json_decode($multiprocessors);

            foreach ($processors as $processor) {
                if ($brand == $processor->brand) {
                    return $processor->issuer;
                }
            }
        }

        return 0;
    }

    /**
     * @param $code
     * @return mixed|null
     */
    public function getBrand($code)
    {
        if (isset($this->_brands[$code])) {
            return $this->_brands[$code];
        }

        return null;
    }

    /**
     * @param $total
     * @param $installments
     * @param string $code
     * @return float|int
     */
    public function getInstallmentValue($total, $installments, $code = 'maxipago_payment_cc')
    {
        $installmentsWithoutInterestRate = (int) $this->scopeConfig->getValue('payment/maxipago_payment/' . $code . '/installments_without_interest_rate');

        $interestRate = $this->scopeConfig->getValue('payment/maxipago_payment/' . $code . '/interest_rate');
        $interestRate = (float) (str_replace(',', '.', $interestRate)) / 100;

        if ($installments > 0) {
            $valorParcela = $total / $installments;
        } else {
            $valorParcela = $total;
        }

        try {
            if ($installments > $installmentsWithoutInterestRate && $interestRate > 0 && $installmentsWithoutInterestRate > 0) {
                $interestType = $this->scopeConfig->getValue('payment/maxipago_payment/' . $code . '/interest_type');

                switch ($interestType) {
                    case 'price':
                        $value = $total * (($interestRate * pow((1 + $interestRate), $installments)) / (pow((1 + $interestRate), $installments) - 1));
                        $valorParcela = round($value, 2);
                        break;
                    case 'compound':
                        //M = C * (1 + i)^n
                        $valorParcela = ($total * pow(1 + $interestRate, $installments)) / $installments;
                        break;
                    case 'simple':
                        //M = C * ( 1 + ( i * n ) )
                        $valorParcela = ($total * (1 + ($installments * $interestRate))) / $installments;
                        break;
                }
            }
        } catch (\Exception $e) {
            // create log
        } finally {
            return $valorParcela;
        }
    }

    /**
     * @param bool $forceCustomer
     * @return mixed|null
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getTaxvatValue($forceCustomer = false)
    {
        $quote = $this->checkoutSession->getQuote();

        $taxvatValue = null;
        $showTaxvatField = $this->scopeConfig->getValue('payment/maxipago_payment/show_taxvat_field');

        if (!$showTaxvatField || $forceCustomer) {

            $attributeCode = $this->scopeConfig->getValue('payment/maxipago_payment/cpf_customer_attribute');
            $isCorporate = $this->isCorporate();

            if ($isCorporate) {
                $attributeCode = $this->scopeConfig->getValue('payment/maxipago_payment/cnpj_customer_attribute');
            }

            if ($quote->getCustomer() && $quote->getCustomer()->getId()) {
                $_customer   = $this->_customerRepositoryInterface->getById($quote->getCustomer()->getId());
                $taxvatValue = $_customer->getResource()->getAttribute($attributeCode)->getFrontend()->getValue($_customer);
            } else {
                $taxvatValue = $quote->getData('customer_' . $attributeCode);
            }
        }

        return $taxvatValue;
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function isCorporate()
    {
        $isCorporate = false;

        $attributeCode = $this->scopeConfig->getValue('payment/maxipago_payment/customer_attribute_type');
        $attributeCodeValue = $this->scopeConfig->getValue('payment/maxipago_payment/customer_attribute_type_value_corporate');

        if (
            $attributeCode
            && $attributeCodeValue
        ) {
            $quote = $this->checkoutSession->getQuote();
            if ($quote->getCustomer() && $quote->getCustomer()->getId()) {

                $_customer = $this->_customerRepositoryInterface->getById($quote->getCustomer()->getId());

                $customerType = $_customer->getData($attributeCode);
                $isCorporate  = ($customerType == $attributeCodeValue);

                if (is_numeric($customerType) && !$isCorporate) {
                    $_customerTypeValue = $_customer->getResource()->getAttribute($attributeCode)->getFrontend()->getValue($_customer);
                    $isCorporate = ($customerType == $_customerTypeValue);
                }
            } else {
                $customerType = $quote->getData('customer_' . $attributeCode);
                $isCorporate = ($customerType == $attributeCodeValue);
            }
        }

        return $isCorporate;
    }

    /**
     * @return \Magento\Backend\Model\Session\Quote|\Magento\Checkout\Model\Session
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getSession()
    {
        if ($this->state->getAreaCode() == \Magento\Framework\App\Area::AREA_ADMINHTML) {
            return $this->backendSessionQuote;
        }

        return $this->checkoutSession;
    }
}