<?php

namespace MaxiPago\Payment\Helper;

use MaxiPago\MaxiPago;
use MaxiPago\Auth\OAuth;

class Order extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var
     */
    protected $helper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\HTTP\Header
     */
    protected $_httpHeader;

    /**
     * @var \Magento\Sales\Model\Service\InvoiceService
     */
    protected $_invoiceService;

    /**
     * @var \Magento\Framework\DB\Transaction
     */
    protected $_transaction;

    /**
     * @var \Magento\Sales\Model\Order\Email\Sender\InvoiceSender
     */
    protected $invoiceSender;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $managerInterface;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Sales\Model\Order\CreditmemoFactory
     */
    protected $creditmemoFactory;

    /**
     * @var \Magento\Sales\Model\Service\CreditmemoService
     */
    protected $creditmemoService;

    /**
     * @var \Magento\Framework\DB\TransactionFactory
     */
    protected $transactionFactory;

    /**
     * Order constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\HTTP\Header $httpHeader
     * @param Data $helper
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Sales\Model\Service\InvoiceService $invoiceService
     * @param \Magento\Framework\DB\Transaction $transaction
     * @param \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender
     * @param \Magento\Framework\Message\ManagerInterface $managerInterface
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Sales\Model\Order\CreditmemoFactory $creditmemoFactory
     * @param \Magento\Sales\Model\Service\CreditmemoService $creditmemoService
     * @param \Magento\Framework\DB\TransactionFactory $transactionFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\HTTP\Header $httpHeader,
        Data $helper,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Framework\DB\Transaction $transaction,
        \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender,
        \Magento\Framework\Message\ManagerInterface $managerInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\Order\CreditmemoFactory $creditmemoFactory,
        \Magento\Sales\Model\Service\CreditmemoService $creditmemoService,
        \Magento\Framework\DB\TransactionFactory $transactionFactory
    )
    {
        parent::__construct($context);

        $this->_httpHeader = $httpHeader;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->_invoiceService = $invoiceService;
        $this->_transaction  = $transaction;
        $this->invoiceSender = $invoiceSender;
        $this->managerInterface = $managerInterface;
        $this->scopeConfig = $scopeConfig;
        $this->creditmemoFactory = $creditmemoFactory;
        $this->creditmemoService = $creditmemoService;
        $this->transactionFactory = $transactionFactory;
    }

    /**
     * @param $payment
     * @return array
     */
    public function getOrderData($payment)
    {
        $orderData = array();

        $order = $payment->getOrder();
        $items = $order->getAllVisibleItems();

        $countItems = count($items);
        if ($countItems > 0) {

            $orderData['itemCount'] = $countItems;
            $i = 1;

            foreach ($items as $item) {
                $qty = $item->getQtyOrdered();
                $price = number_format($item->getPrice(), 2, '.', '');
                $orderData['itemIndex' . $i] = $i;
                $orderData['itemProductCode' . $i] = $item->getSku();
                $orderData['itemDescription' . $i] = $item->getName();
                $orderData['itemQuantity' . $i] = $qty;
                $orderData['itemUnitCost' . $i] = $price;
                $orderData['itemTotalAmount' . $i] = number_format($qty * $price, 2, '.', '');

                $i++;
            }
        }

        $orderData['userAgent'] = $this->_httpHeader->getHttpUserAgent();

        return $orderData;
    }

    /**
     * @param $order
     * @param null $transactionId
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createInvoice($order, $transactionId = null)
    {
        if ($order->canInvoice()) {
            $payment = $order->getPayment();

            if (!$transactionId) {
                $transactionId = $payment->getAdditionalInformation('transaction_id');
            }

            $invoice = $this->_invoiceService->prepareInvoice($order);

            $invoice->register();

            $invoice->save();

            $transactionSave = $this->_transaction->addObject(
                $invoice
            )->addObject(
                $invoice->getOrder()
            );

            $transactionSave->save();

            $this->invoiceSender->send($invoice);

            $status = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/captured_order_status');

            if ($status) {
                $message = __('The payment was confirmed - Transaction ID: ') . (string) $transactionId;

                $order->addStatusHistoryComment($message, $status)
                    ->setIsCustomerNotified(true)
                    ->save();
            }
        }
    }

    /**
     * @param $order
     * @param null $transactionId
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function cancelOrder($order, $transactionId = null)
    {
        $payment = $order->getPayment();

        $order = $payment->getOrder();

        if (!$order->canCancel() && !$order->canCreditmemo()) {
            if ($order->getState() != \Magento\Sales\Model\Order::STATE_CANCELED) {
                $payment->setIsTransactionDenied(true);
                $payment->setAdditionalInformation('cancelled', true);

                foreach ($order->getInvoiceCollection() as $invoice) {
                    $invoice = $invoice->load($invoice->getId());

                    if ($invoice) {
                        $invoice->cancel();
                        $order->addRelatedObject($invoice);
                        $invoice->save();
                    }

                    $message = __('Registered update about denied payment.');
                    $order->registerCancellation($message, false);
                }

                $order->save();
            }
        } else if ($order->canCancel()) {
            $order->cancel();
            $order->addStatusHistoryComment('Order cancelled at maxiPago!', false);
            $payment->setAdditionalInformation('cancelled', true);
            $payment->save;
            $order->save();
        } else if ($order->canCreditmemo()) {
            $creditmemo = $this->creditmemoFactory->createByOrder($order);

            $creditmemo->setOfflineRequested(true);

            $this->creditmemoService->refund($creditmemo);

            $autoReturnOption = $this->scopeConfig->getValue(\Magento\CatalogInventory\Model\Configuration::XML_PATH_ITEM_AUTO_RETURN);

            foreach ($creditmemo->getAllItems() as $creditmemoItem) {
                if ($autoReturnOption) {
                    $creditmemoItem->setBackToStock(true);
                }
            }

            $payment->setAdditionalInformation('cancelled', true);

            $transaction = $this->transactionFactory->create()
                ->addObject($creditmemo)
                ->addObject($payment)
                ->addObject($creditmemo->getOrder());

            if ($creditmemo->getInvoice()) {
                $transaction->addObject($creditmemo->getInvoice());
            }

            $transaction->save();
        }

        $payment->save();
    }

    /**
     * @param $order
     * @param $record
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function updatePayment($order, $record)
    {
        $helper = $this->helper;

        $transactionId = isset($record['transactionId']) ? $record['transactionId'] : null;
        $state  = isset($record['transactionState']) ? $record['transactionState'] : null;
        $amount = isset($record['transactionAmount']) ? $record['transactionAmount'] : null;

        $message = __('No updates available');

        if ($state) {
            $lastTransactionState = $order->getPayment()->getAdditionalInformation('last_transaction_state');

            if ($lastTransactionState != $state) {
                if ($state == '10' || $state == '3') {

                    $this->createInvoice($order, $transactionId);
                    $message = __('Order approved, TID %s', $transactionId);

                } else if ($state == '45' || $state == '7' || $state == '9') {

                    $this->cancelOrder($order, $transactionId);
                    $message = __('Order cancelled, TID %s', $transactionId);

                } else {
                    $transactionStatus = $helper->getTransactionState($state);
                    $message = __('Order synchronized with status <strong>%s</strong>', $transactionStatus);

                    $order->addStatusHistoryComment($message);
                    $order->save();

                }

                $order->getPayment()->setAdditionalInformation('last_transaction_state', $state);
                $order->getPayment()->save();
            }

        }

        $this->managerInterface->addNoticeMessage($message);
    }
}