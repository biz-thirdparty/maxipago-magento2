<?php

namespace MaxiPago\Payment\Helper;

class Api extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \MaxiPago\Payment\Logger\Logger
     */
    protected $maxipagoLogger;

    /**
     * @var \MaxiPago\Payment\lib\MaxiPago
     */
    protected $libMaxiPago;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \MaxiPago\Payment\Helper\Data
     */
    protected $_maxipagoHelper;

    /**
     * Api constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \MaxiPago\Payment\Logger\Logger $maxipagoLogger
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \MaxiPago\Payment\lib\MaxiPago $libMaxiPago
     * @param \Psr\Log\LoggerInterface $logger
     * @param Data $maxipagoHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \MaxiPago\Payment\Logger\Logger $maxipagoLogger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \MaxiPago\Payment\lib\MaxiPago $libMaxiPago,
        \Psr\Log\LoggerInterface $logger,
        \MaxiPago\Payment\Helper\Data $maxipagoHelper
    )
    {
        parent::__construct($context);

        $this->maxipagoLogger = $maxipagoLogger;
        $this->scopeConfig = $scopeConfig;
        $this->libMaxiPago = $libMaxiPago;
        $this->logger = $logger;
        $this->_maxipagoHelper = $maxipagoHelper;
    }

    /**
     * @throws \Exception
     */
    public function authenticate()
    {
        $merchantId = $this->scopeConfig->getValue('payment/maxipago_payment/id');
        $merchantKey = $this->scopeConfig->getValue('payment/maxipago_payment/store_key');
        $merchantSecret = $this->scopeConfig->getValue('payment/maxipago_payment/secret_store_key');

        if ($merchantId && $merchantSecret) {
            $environment = $this->scopeConfig->getValue('payment/maxipago_payment/test_environment');
            $this->libMaxiPago->setCredentials($merchantId, $merchantKey);
            $this->libMaxiPago->setEnvironment($environment);
        }
    }

    /**
     * @param $order
     * @param bool $transactionId
     * @return bool
     */
    public function pullReport($order, $transactionId = false)
    {
        try {
            $payment = $order->getPayment();

            $this->authenticate();

            if ($order->getPayment()->getMethod() == 'maxipago_checkout2') {
                $data = array(
                    "payOrderId" => $payment->getAdditionalInformation('pay_order_id'),
                );

                $this->libMaxiPago->pullPaymentOrder($data);
            } else {
                if ($transactionId) {
                    $data = array(
                        "transactionID" => $payment->getAdditionalInformation('transaction_id'),
                    );
                } else {
                    $data = array(
                        "orderID" => $payment->getAdditionalInformation('order_id'),
                    );
                }

                $this->libMaxiPago->pullReport($data);
            }

            $response = $this->libMaxiPago->response;

            $this->_maxipagoHelper->debug($this->libMaxiPago);

            return $response;

        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }

        return false;
    }

    /**
     * @param $orderId
     * @param null $pageToken
     * @param null $pageNumber
     * @return bool
     */
    public function pullReportByOrderId($orderId, $pageToken = null, $pageNumber = null)
    {
        try {
            $data = array(
                "orderID" => $orderId,
            );

            if ($pageToken) {
                $data['pageToken'] = $pageToken;
            }

            if ($pageNumber) {
                $data['pageNumber'] = $pageNumber;
            }

            $this->authenticate();

            $this->libMaxiPago->pullReport($data);

            $response = $this->libMaxiPago->response;

            return $response;
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }

        return false;
    }

    /**
     * @param $order
     * @param $transactionId
     * @return bool
     */
    public function void($order, $transactionId)
    {
        try {
            $data = array(
                'transactionID' => $transactionId
            );

            $this->authenticate();
            
            $this->libMaxiPago->creditCardVoid($data);
            $response = $this->libMaxiPago->response;

            $this->_maxipagoHelper->debug($this->libMaxiPago);

            if (
                (isset($response['errorMessage']) && $response['errorMessage'])
                ||
                (isset($response['errorMsg']) && $response['errorMsg'])
            ) {
                $error = isset($response['errorMessage']) ? $response['errorMessage'] : $response['errorMsg'];
                $this->logger->critical($error);
            }

            return $response;

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        return false;
    }

    /**
     * @param $order
     * @param $maxipagoOrderId
     * @param $amount
     * @return bool
     */
    public function refund($order, $maxipagoOrderId, $amount)
    {
        try {
            $amount = number_format($amount, 2, '.', '');
            $data = array(
                'orderID'      => $maxipagoOrderId,
                'referenceNum' => $order->getIncrementId(),
                'chargeTotal'  => $amount,
            );

            $this->authenticate();

            $this->libMaxiPago->creditCardRefund($data);
            $response = $this->libMaxiPago->response;

            $this->_maxipagoHelper->debug($this->libMaxiPago);

            if (
                (isset($response['errorMessage']) && $response['errorMessage'])
                ||
                (isset($response['errorMsg']) && $response['errorMsg'])
            ) {
                $error = isset($response['errorMessage']) ? $response['errorMessage'] : $response['errorMsg'];
                $this->logger->critical($error);
            }

            return $response;

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        return false;
    }
}