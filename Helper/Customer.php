<?php

namespace MaxiPago\Payment\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\App\Action;
use MaxiPago\MaxiPago;
use MaxiPago\Auth\OAuth;

class Customer extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Data
     */
    protected $_helper;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepositoryInterface;

    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    protected $_addressRepositoryInterface;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $_countryFactory;

    /**
     * @var \MaxiPago\Payment\lib\MaxiPago
     */
    protected $libMaxiPago;

    /**
     * @var \MaxiPago\Payment\Logger\Logger
     */
    protected $maxipagoLogger;

    /**
     * @var \Magento\Framework\Filter\FilterManager 
     */
    protected $filterManager;

    /**
     * @var \MaxiPago\Payment\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManagerInterface;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var Api
     */
    protected $helperApi;

    /**
     * @var \MaxiPago\Payment\Helper\Data
     */
    protected $_maxipagoHelper;

    /**
     * Customer constructor.
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Magento\Customer\Api\AddressRepositoryInterface $addressRepositoryInterface
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param Data $helper
     * @param \MaxiPago\Payment\lib\MaxiPago $libMaxiPago
     * @param \MaxiPago\Payment\Logger\Logger $maxipagoLogger
     * @param \Magento\Framework\Filter\FilterManager $filterManager
     * @param \MaxiPago\Payment\Model\CustomerFactory $customerFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManagerInterface
     * @param \Psr\Log\LoggerInterface $logger
     * @param Api $helperApi
     * @param Data $maxipagoHelper
     */
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepositoryInterface,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \MaxiPago\Payment\Helper\Data $helper,
        \MaxiPago\Payment\lib\MaxiPago $libMaxiPago,
        \MaxiPago\Payment\Logger\Logger $maxipagoLogger,
        \Magento\Framework\Filter\FilterManager $filterManager,
        \MaxiPago\Payment\Model\CustomerFactory $customerFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Psr\Log\LoggerInterface $logger,
        \MaxiPago\Payment\Helper\Api $helperApi,
        \MaxiPago\Payment\Helper\Data $maxipagoHelper
    )
    {
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_addressRepositoryInterface = $addressRepositoryInterface;
        $this->_countryFactory = $countryFactory;
        $this->_helper = $helper;
        $this->maxipagoLogger = $maxipagoLogger;
        $this->libMaxiPago = $libMaxiPago;
        $this->filterManager = $filterManager;
        $this->customerFactory = $customerFactory;
        $this->scopeConfig = $scopeConfig;
        $this->storeManagerInterface = $storeManagerInterface;
        $this->logger = $logger;
        $this->helperApi = $helperApi;
        $this->_maxipagoHelper = $maxipagoHelper;
    }

    protected $_regions = [
        'ACRE'                => 'AC',
        'ALAGOAS'             => 'AL',
        'AMAPÁ'               => 'AP',
        'AMAZONAS'            => 'AM',
        'BAHIA'               => 'BA',
        'CEARÁ'               => 'CE',
        'DISTRITO FEDERAL'    => 'DF',
        'ESPÍRITO SANTO'      => 'ES',
        'GOIÁS'               => 'GO',
        'MARANHÃO'            => 'MA',
        'MATO GROSSO'         => 'MT',
        'MATO GROSSO DO SUL'  => 'MS',
        'MINAS GERAIS'        => 'MG',
        'PARÁ'                => 'PA',
        'PARAÍBA'             => 'PB',
        'PARANÁ'              => 'PR',
        'PERNAMBUCO'          => 'PE',
        'PIAUÍ'               => 'PI',
        'RIO DE JANEIRO'      => 'RJ',
        'RIO GRANDE DO NORTE' => 'RN',
        'RIO GRANDE DO SUL'   => 'RS',
        'RONDÔNIA'            => 'RO',
        'RORAIMA'             => 'RR',
        'SANTA CATARINA'      => 'SC',
        'SÃO PAULO'           => 'SP',
        'SERGIPE'             => 'SE',
        'TOCATINS'            => 'TO'
    ];

    /**
     * @param $customerId
     * @param $payment
     * @param string $type
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getAddressData($customerId, $payment, $type = 'billing')
    {
        $customer = $this->_customerRepositoryInterface->getById($customerId);

        $data = array();
        $data[$type . 'Id']        = $customer->getId();
        $data[$type . 'Name']      = $customer->getFirstname() . ' ' . $customer->getLastname();
        $data[$type . 'Email']     = $customer->getEmail();
        $data[$type . 'BirthDate'] = $this->_helper->getDate($customer->getDob());
        $data[$type . 'Gender']    = $customer->getGender() ? 'M' : 'F';
        $documentNumber = $this->digits($payment->getAdditionalInformation('cpf_cnpj'));
        $customerType   = 'Individual';
        $documentType   = 'CPF';

        if (strlen($documentNumber) == '14') {
            $customerType = 'Legal entity';
            $documentType = 'CNPJ';
        }

        $data[$type . 'Type'] = $customerType;
        $data[$type . 'DocumentType'] = $documentType;
        $data[$type . 'DocumentValue'] = $documentNumber;


        if ($type == 'billing') {
            $billingAddressId = $customer->getDefaultBilling();
            $address = $this->_addressRepositoryInterface->getById($billingAddressId);
        } else {
            $shippingAddressId = $customer->getDefaultShipping();
            $address = $this->_addressRepositoryInterface->getById($shippingAddressId);
        }

        if (!$address) {
            $address = $customer->getAddresses()[0];
        }

        if ($address) {
            $regionCode = $address->getRegion()->getRegionCode();
            $telephone  = $this->digits($address->getTelephone());

            $data[$type . 'PhoneType']     = 'Mobile';
            $data[$type . 'CountryCode']   = $this->_helper->getCountryCode($address->getCountryId());
            $data[$type . 'PhoneAreaCode'] = $this->_helper->getAreaNumber($telephone);
            $data[$type . 'PhoneNumber']   = $this->_helper->getPhoneNumber($telephone);
            $data[$type . 'Address']       = $address->getStreet()[0];
            $data[$type . 'Address2']      = $address->getStreet()[1];
            $data[$type . 'District']      = $address->getStreet()[2];
            $data[$type . 'City']          = $address->getCity();
            $data[$type . 'State']         = $regionCode;
            $data[$type . 'Zip']           = $address->getPostcode();
            $data[$type . 'PostalCode']    = $address->getPostcode();
            $data[$type . 'Country']       = $address->getCountryId();
        }

        return $data;
    }

    /**
     * @param $customerId
     * @param string $format
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCustomerData($customerId, $format = 'd/m/Y')
    {
        $customer = $this->_customerRepositoryInterface->getById($customerId);

        $data = array(
            'name'      => $customer->getFirstname() . ' ' . $customer->getLastname(),
            'email'     => $customer->getEmail(),
            'firstName' => $customer->getFirstname(),
            'lastName'  => $customer->getLastname(),
            'sex'       => $customer->getGender() ? 'M' : 'F',
            'dob'       => $this->_helper->getDate($customer->getDob(), $format)
        );

        $billingAddressId = $customer->getDefaultBilling();
        $address = $this->_addressRepositoryInterface->getById($billingAddressId);

        if (!$address) {
            $address = $customer->getAddresses()[0];
        }

        if ($address) {
            $regionCode = $regionCode = $address->getRegion()->getRegionCode();

            $telephone = $this->digits($address->getTelephone());
            $mobile = $this->digits($address->getFax());

            $street = $this->getStreet($address->getStreet());

            $data['phone']      = $telephone;
            $data['mobile']     = $mobile;
            $data['address1']   = $street['address1'];
            $data['address2']   = $street['address2'];
            $data['district']   = $street['district'];
            $data['city']       = $address->getCity();
            $data['state']      = $regionCode;
            $data['postalcode'] = $this->digits($address->getPostcode());
            $data['zip']        = $this->digits($address->getPostcode());
            $data['country']    = $address->getCountryId();
        }

        return $data;
    }

    /**
     * @param $email
     * @return \Magento\Customer\Api\Data\CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCustomerByEmail($email)
    {
        return $this->_customerRepositoryInterface->get($email, $this->storeManagerInterface->getStore()->getWebsiteId());
    }

    /**
     * @param $string
     * @return null|string|string[]
     */
    protected function digits($string)
    {
        return preg_replace('/[^0-9]/', '', $string);
    }

    /**
     * @param $config
     * @return mixed
     */
    protected function getConfigData($config)
    {
        return $this->scopeConfig->getValue('payment/maxipago_payment/' . $config);
    }

    /**
     * @param $street
     * @return array
     */
    protected function getStreet($street)
    {
        if (is_string($street)) {
            $street = explode("\n", $street);
        }

        $address = array();

        $address['address1'] = $street[0];
        $address['address2'] = isset($street[1]) && !empty($street[1]) ? $street[1] : 'N/A';
        $address['district'] = 'N/A';

        if (count($street) == 3) {
            $address['district'] = $street[2];
        } else if (count($street) == 4) {
            $address['address2'] = $street[1] . ' ' . $street[2];
            $address['district'] = $street[3];
        }

        return $address;
    }

    /**
     * @param $address
     * @return mixed|string
     */
    protected function getRegionCode($address)
    {
        $regionCode = $address->getRegionCode();

        if (!$regionCode || strlen($regionCode) != 2) {
            $regionName = trim($address->getRegion());
            $regionCode = $regionName;

            if (strlen($regionName) != 2) {
                $regionName = strtoupper($regionName);
                $regionCode = isset($this->_regions[$regionName])
                    ? $this->_regions[$regionName]
                    : $this->decipherRegion($regionName);
            }
        }

        return $regionCode;
    }

    /**
     * @param $region
     * @return string
     */
    public function decipherRegion($region)
    {
        $code = '';
        $region = strtoupper($this->filterManager->removeAccents($region));

        switch ($region) {
            case 'ACRE':
                $code = 'AC';
                break;
            case 'ALAGOAS':
                $code = 'AL';
                break;
            case 'AMAPA':
            case 'AMAP':
                $code = 'AP';
                break;
            case 'AMAZONAS':
            case 'AMAZONA':
                $code = 'AM';
                break;
            case 'BAHIA':
            case 'BAIA':
                $code = 'BA';
                break;
            case 'CEARA':
                $code = 'CE';
                break;
            case 'DISTRITO FEDERAL':
                $code = 'DF';
                break;
            case 'ESPIRITO SANTO':
                $code = 'ES';
                break;
            case 'GOIAS':
                $code = 'GO';
                break;
            case 'MARANHAO':
                $code = 'MA';
                break;
            case 'MATO GROSSO':
                $code = 'MT';
                break;
            case 'MATO GROSSO DO SUL':
                $code = 'MS';
                break;
            case 'MINAS GERAIS':
            case 'MINAS':
                $code = 'MG';
                break;
            case 'PARA':
                $code = 'PA';
                break;
            case 'PARAIBA':
                $code = 'PB';
                break;
            case 'PARANA':
                $code = 'PR';
                break;
            case 'PERNAMBUCO':
            case 'PERNANBUCO':
                $code = 'PE';
                break;
            case 'PIAUI':
                $code = 'PI';
                break;
            case 'RIO DE JANEIRO':
            case 'RIO JANEIRO':
            case 'RIO':
                $code = 'RJ';
                break;
            case 'RIO GRANDE DO NORTE':
                $code = 'RN';
                break;
            case 'RIO GRANDE DO SUL':
                $code = 'RS';
                break;
            case 'RONDONIA':
            case 'RONDONA':
                $code = 'RO';
                break;
            case 'RORAIMA':
                $code = 'RR';
                break;
            case 'SANTA CATARINA':
                $code = 'SC';
                break;
            case 'SAO PAULO':
                $code = 'SP';
                break;
            case 'SERGIPE':
                $code = 'SE';
                break;
            case 'TOCANTINS':
                $code = 'TO';
                break;
        }

        return $code;
    }

    /**
     * @param $order
     * @return mixed
     */
    public function getMaxipagoCustomer($order)
    {
        try {
            $mpCustomer = $this->customerFactory->create()->getCollection()
                ->addFieldToFilter('customer_id', $order->getCustomerId());

            $customer = $mpCustomer->getFirstItem();

            if (!$customer->getId()) {
                $customerData = [
                    'customerIdExt' => $order->getCustomerId(),
                    'firstName'     => $order->getCustomerFirstname(),
                    'lastName'      => $order->getCustomerLastname()
                ];

                $this->helperApi->authenticate();

                $this->libMaxiPago->addProfile($customerData);

                $response = $this->libMaxiPago->response;

                $this->_maxipagoHelper->debug($this->libMaxiPago);

                if ($response['errorCode'] != 1) {
                    $mpCustomerId = $this->libMaxiPago->getCustomerId();

                    $customer->setData('customer_id', $order->getCustomerId());
                    $customer->setData('customer_id_maxipago', $mpCustomerId);

                    $customer->save();
                }
            }

            return $customer;
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}