<?php

namespace MaxiPago\Payment\Controller\Sales;

class Payment extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Magento\Sales\Model\Order', 'Magento\Sales\Model\ResourceModel\Order');
    }

    /**
     * @param $paymentMethod
     */
    protected function filterOrder($paymentMethod, $ninStatuses, $fromDate)
    {
        $this->sales_order_table = "main_table";
        $this->sales_order_payment_table = $this->getTable("sales_order_payment");

        $this->getSelect()
            ->join(array('payment' => $this->sales_order_payment_table), $this->sales_order_table . '.entity_id= payment.parent_id',
                array('payment_method' => 'payment.method',
                    'order_id' => $this->sales_order_table . '.entity_id'
                )
            );
//
//        $collection = Mage::getModel('sales/order')->getCollection()
//            ->join(
//                array('payment' => 'sales/order_payment'),
//                'main_table.entity_id=payment.parent_id',
//                array('payment_method' => 'payment.method')
//            )
//            ->addFieldToFilter('payment.method', array('like' => 'maxipago_%'))
//            ->addFieldToFilter('state', array('nin' => array($ninStatuses)))
//            ->addFieldToFilter('created_at', array('gt' => $fromDate))
//        ;

        $this->getSelect()->where("payment_method LIKE maxipago_%" . $paymentMethod);
        $this->getSelect()->where("main_table.state =" . $ninStatuses);
        $this->getSelect()->where("main_table.created_at=" . $fromDate);
    }
}