<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MaxiPago\Payment\Block\Info;

class Ticket extends \Magento\Payment\Block\Info
{
    protected $_template = 'MaxiPago_Payment::info/ticket.phtml';

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCpfCnpj()
    {
        $_info = $this->getInfo();
        $transactionId = $_info->getAdditionalInformation('cpf_cnpj');

        return $transactionId;
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getLinkPrintPay()
    {
        $_info = $this->getInfo();
        $transactionId = $_info->getAdditionalInformation('ticket_url');

        return $transactionId;
    }
}