<?php

namespace MaxiPago\Payment\Block\Adminhtml\Form\Field;

class Processor extends \Magento\Framework\View\Element\Html\Select
{
    /**
     * methodList
     *
     * @var array
     */
    protected $groupfactory;

    /**
     * @var
     */
    protected $systemConfig;

    protected $scopeConfig;

    /**
     * Processor constructor.
     * @param \Magento\Framework\View\Element\Context $context
     * @param \Magento\Customer\Model\GroupFactory $groupfactory
     * @param \MaxiPago\Payment\Model\System\Config\Manager\Processor $systemConfig
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Magento\Customer\Model\GroupFactory $groupfactory,
        \MaxiPago\Payment\Model\System\Config\Manager\Processor $systemConfig,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    )
    {
        parent::__construct($context, $data);

        $this->groupfactory = $groupfactory;
        $this->systemConfig = $systemConfig;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml()
    {
        if (!$this->getOptions()) {
            foreach ($this->systemConfig->toOptionArray() as $value) {
                $this->addOption($value['value'], $value['label']);
            }
        }

        return parent::_toHtml();
    }

    /**
     * Sets name for input element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }
}