<?php

namespace MaxiPago\Payment\Block\System\Form\Field;

class Active extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    /**
     * Grid columns
     *
     * @var array
     */
    protected $_columns = [];

    /**
     * @var
     */
    protected $ccType;

    /**
     * @var
     */
    protected $processor;

    /**
     * Enable the "Add after" button or not
     *
     * @var bool
     */
    protected $_addAfter = true;

    /**
     * Label of add button
     *
     * @var string
     */
    protected $_addButtonLabel;

    /**
     * Check if columns are defined, set template
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_addButtonLabel = __('Add');
    }

    /**
     * @return \Magento\Framework\View\Element\BlockInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getCcTypeRenderer()
    {
        if (!$this->ccType) {
            $this->ccType = $this->getLayout()->createBlock(
                '\MaxiPago\Payment\Block\Adminhtml\Form\Field\CcType', '', ['data' => ['is_render_to_js_template' => true]]
            );
        }

        return $this->ccType;
    }

    /**
     * @return \Magento\Framework\View\Element\BlockInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getProcessorRenderer()
    {
        if (!$this->processor) {
            $this->processor = $this->getLayout()->createBlock(
                '\MaxiPago\Payment\Block\Adminhtml\Form\Field\Processor', '', ['data' => ['is_render_to_js_template' => true]]
            );
        }

        return $this->processor;
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareToRender()
    {
        $this->addColumn(
            'brand', [
                'label' => __('Brand'),
                'renderer' => $this->getCcTypeRenderer(),
            ]
        );

        $this->addColumn(
            'issuer', [
                'label' => __('Issuer'),
                'renderer' => $this->getProcessorRenderer(),
            ]
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    /**
     * @param \Magento\Framework\DataObject $row
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $brand = $row->getBrand();
        $options = [];

        if ($brand) {
            $options['option_' . $this->getCcTypeRenderer()->calcOptionHash($brand)] = 'selected="selected"';
        }

        $row->setData('option_extra_attrs', $options);
    }

    /**
     * @param string $columnName
     * @return string
     * @throws \Exception
     */
    public function renderCellTemplate($columnName)
    {
        if ($columnName == "issuer") {
            $this->_columns[$columnName]['class'] = 'input-text required-entry validate-number';
            $this->_columns[$columnName]['style'] = 'width:50px';
        }

        return parent::renderCellTemplate($columnName);
    }
}