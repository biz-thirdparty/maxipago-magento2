define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,
              rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'maxipago_payment_tef',
                component: 'MaxiPago_Payment/js/view/payment/method-renderer/maxipago_payment_tef'
            }
        );
        return Component.extend({});
    }
);