define(
    [
        'underscore',
        'jquery',
        'ko',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/action/place-order',
        'Magento_Checkout/js/action/select-payment-method',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/checkout-data',
        'Magento_Payment/js/model/credit-card-validation/credit-card-data',
        'Magento_Payment/js/model/credit-card-validation/validator',
        'Magento_Checkout/js/model/payment/additional-validators',
        'mage/url',
        'mage/calendar',
        'mage/translate'
    ],
    function (_,
              $,
              ko,
              quote,
              priceUtils,
              Component,
              placeOrderAction,
              selectPaymentMethodAction,
              customer,
              checkoutData,
              creditCardData,
              validator,
              additionalValidators,
              cardNumberValidator,
              custom,
              url,
              calendar) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'MaxiPago_Payment/payment/maxipago_payment_ticket'
            },

            isActive: function () {
                return parseInt(window.checkoutConfig.payment.maxipago_payment_ticket.active);
            },

            /** Returns send check to info */
            getInstruction: function () {
                return window.checkoutConfig.payment.maxipago_payment_ticket.instruction;
            },

            /** Returns payable to info */
            getDue: function () {
                return window.checkoutConfig.payment.maxipago_payment_ticket.due;
            },

            getBanksActive: function () {
                return 1;
            },

            getBanks: function () {
                var banks = {
                    11: "Itaú"
                    , 12: "Bradesco"
                    , 13: "Banco do Brasil"
                    , 14: "HSBC"
                    , 15: "Santander"
                    , 16: "Caixa Econômica Federal"
                };

                var newArray = [];

                function eachElements(element, index, array) {
                    newArray.push({"value": element, "banks": banks[element]});
                }

                window.checkoutConfig.payment.maxipago_payment_ticket.banks.forEach(eachElements);

                return newArray;
            },

            selectPaymentMethod: function () {
                selectPaymentMethodAction(this.getData());
                checkoutData.setSelectedPaymentMethod('maxipago_payment_ticket');
                return true;
            },

            getData: function () {
                return {
                    'method': 'maxipago_payment_ticket',
                    'additional_data': {
                        'bank': jQuery('#' + this.getCode() + '_bank').val(),
                        'cpf_cnpj': jQuery('#' + this.getCode() + '_cpf').val()
                    }
                };
            },

            getCode: function () {
                return 'maxipago_payment_ticket';
            }
        });
    }
);