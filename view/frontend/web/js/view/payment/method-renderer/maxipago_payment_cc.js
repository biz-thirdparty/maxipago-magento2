define(
    [
        'underscore',
        'jquery',
        'ko',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/action/place-order',
        'Magento_Checkout/js/action/select-payment-method',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/checkout-data',
        'Magento_Payment/js/model/credit-card-validation/credit-card-data',
        'Magento_Payment/js/model/credit-card-validation/validator',
        'Magento_Checkout/js/model/payment/additional-validators',
        'mage/url',
        'mage/calendar',
        'mage/translate'
    ],
    function (
        _,
        $,
        ko,
        quote,
        priceUtils,
        Component,
        placeOrderAction,
        selectPaymentMethodAction,
        customer,
        checkoutData,
        creditCardData,
        validator,
        additionalValidators,
        cardNumberValidator,
        custom,
        url,
        calendar
    ) {
        'use strict';
 
        return Component.extend({
            defaults: {
                template: 'MaxiPago_Payment/payment/maxipago_payment_cc'
            },

            /** Returns send check to info */
            getInstruction: function() {
                return window.checkoutConfig.payment.maxipago_payment_cc.instruction;
            },

            /** Returns payable to info */
            getDue: function() {
                return window.checkoutConfig.payment.maxipago_payment_cc.due;
            },

            isActive: function () {
                return parseInt(window.checkoutConfig.payment.maxipago_payment_cc.active);
            },

            getCcType: function () {
                return [
                    {"value": "VI", "cc_type": "Visa"},
                    {"value": "MC", "cc_type": "Mastercard"},
                    {"value": "AM", "cc_type": "Amex"},
                    {"value": "DC", "cc_type": "Diners Club"},
                    {"value": "EL", "cc_type": "Elo"},
                    {"value": "DI", "cc_type": "Discover"},
                    {"value": "HC", "cc_type": "Hipercard"},
                    {"value": "JC", "cc_type": "JCB"},
                    {"value": "AU", "cc_type": "Aura"},
                    {"value": "CR", "cc_type": "Credz"}
                ];
            },

            getInstallments: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.installments;
            },

            getCcYearsValues: function () {
                return _.map(window.checkoutConfig.payment.maxipago_payment_cc.years, function (value, key) {
                    return {
                        'value': key,
                        'year': value
                    };
                });
            },

            getCcMonthsValues: function () {
                return _.map(window.checkoutConfig.payment.maxipago_payment_cc.months, function (value, key) {
                    return {
                        'value': key,
                        'month': value
                    };
                });
            },

            selectPaymentMethod: function() {
                selectPaymentMethodAction(this.getData());
                checkoutData.setSelectedPaymentMethod('maxipago_payment_cc');
                return true;
            },

            getData: function () {
                return {
                    'method': 'maxipago_payment_cc',
                    'additional_data': {
                        'cc_type'          : jQuery('.maxipago_cc_type').val(),
                        'cc_number'        : jQuery('#' + this.getCode() + '_cc_number').val(),
                        'cc_owner'         : jQuery('#' + this.getCode() + '_cc_owner').val(),
                        'month'            : jQuery('#' + this.getCode() + '_month').val(),
                        'year'             : jQuery('#' + this.getCode() + '_year').val(),
                        'cc_cid'           : jQuery('#' + this.getCode() + '_cc_cid').val(),
                        'cpf_cnpj'         : jQuery('#' + this.getCode() + '_cpf_cnpj').val(),
                        'installments'     : jQuery('#' + this.getCode() + '_installments').val(),
                        'save_card'        : jQuery('#' + this.getCode() + '_save_card').val(),
                        'token'            : jQuery('#' + this.getCode() + '_cc_token').val(),
                        'use_saved_card'   : jQuery('#maxipagoUseSavedCard').val()
                    }
                };
            },

            getCode: function() {
                return 'maxipago_payment_cc';
            },

            getCcToken: function() {
                return window.checkoutConfig.payment.maxipago_payment_cc.tokens;
            },

            getSavedCard: function () {
                jQuery("#maxipago_payment_cc_form div.token").show();
                jQuery("#maxipago_payment_cc_form div.brand").hide();
                jQuery("#maxipago_payment_cc_form div.number").hide();
                jQuery("#maxipago_payment_cc_form div.name").hide();
                jQuery("#maxipago_payment_cc_form div.month").hide();
                jQuery("#useSavedCardButton").hide();
                jQuery("#useNewCardButton").show();
            },

            getNewCard: function () {
                jQuery("#maxipago_payment_cc_form div.brand").show();
                jQuery("#maxipago_payment_cc_form div.number").show();
                jQuery("#maxipago_payment_cc_form div.name").show();
                jQuery("#maxipago_payment_cc_form div.month").show();
                jQuery("#maxipago_payment_cc_form div.token").hide();
                jQuery("#useSavedCardButton").show();
                jQuery("#useNewCardButton").hide();
            },

            useSavedCard: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.use_saved_card;
            },

            isActiveAmex: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.active_icons.amex;
            },

            isActiveAura: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.active_icons.aura;
            },

            isActiveCredz: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.active_icons.credz;
            },

            isActiveDiners: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.active_icons.diners;
            },

            isActiveDinersClub: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.active_icons.diners_club;
            },

            isActiveDiscover: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.active_icons.discover;
            },

            isActiveElo: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.active_icons.elo;
            },

            isActiveHiper: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.active_icons.hiper;
            },

            isActiveHipercard: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.active_icons.hipercard;
            },

            isActiveJbs: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.active_icons.jbs;
            },

            isActiveJcb: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.active_icons.jcb;
            },

            isActiveMaster: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.active_icons.master;
            },

            isActiveVisa: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.active_icons.visa;
            },

            getIconAmex: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.icons.amex;
            },

            getIconAura: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.icons.aura;
            },

            getIconCredz: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.icons.credz;
            },

            getIconDiners: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.icons.diners;
            },

            getIconDinersClub: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.icons.diners_club;
            },

            getIconDiscover: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.icons.discover;
            },

            getIconElo: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.icons.elo;
            },

            getIconHiper: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.icons.hiper;
            },

            getIconHipercard: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.icons.hipercard;
            },

            getIconJbs: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.icons.jbs;
            },

            getIconJcb: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.icons.jcb;
            },

            getIconMaster: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.icons.master;
            },

            getIconVisa: function () {
                return window.checkoutConfig.payment.maxipago_payment_cc.icons.visa;
            },

            changeIdentifyCcNumber: function () {
                jQuery('#payment_form_maxipago_payment_cc input.maxipago_cc_type').each(function(){
                    jQuery(this).parent().find('img').css("border", "none");
                    jQuery(this).parent().find('img').css("opacity", "1");
                });

                var ccType = this.identifyCcNumber(jQuery("#maxipago_payment_cc_cc_number").val());

                jQuery('#payment_form_maxipago_payment_cc input.maxipago_cc_type').each(function(){
                    if (jQuery(this).val() == ccType) {
                        jQuery(this).click();
                        jQuery(this).parent().find('img').css("border", "1px solid #b30000");
                    } else {
                        jQuery(this).parent().find('img').css("opacity", "0.5");
                    }
                });
            },

            identifyCcNumber: function(ccNumber) {
                var creditCard = '';
                var visa = /^4\d{12,15}/;
                var master = /^5[1-5]{1}\d{14}/;
                var amex = /^(34|37)\d{13}/;
                var elo = /^(636368|438935|504175|451416|(6362|5067|4576|4011)\d{2})\d{10}/;
                var discover = /^(6011|622\d{1}|(64|65)\d{2})\d{12}/;
                var hipercard = /^(60\d{2}|3841)\d{9,15}/;
                var diners = /^((30(1|5))|(36|38)\d{1})\d{11}/;
                var jcb = /^(?:2131|1800|35\d{3})\d{11}/;
                var aura = /^50\d{14}/;

                if (visa.test(ccNumber)) {
                    creditCard = 'VI';
                } else if(master.test(ccNumber)) {
                    creditCard = 'MC';
                } else if(amex.test(ccNumber)) {
                    creditCard = 'AM';
                } else if(discover.test(ccNumber)) {
                    creditCard = 'DI';
                } else if(diners.test(ccNumber)) {
                    creditCard = 'DC';
                } else if (elo.test(ccNumber)) {
                    creditCard = 'EL';
                } else if(hipercard.test(ccNumber)) {
                    creditCard = 'HC';
                } else if(jcb.test(ccNumber)) {
                    creditCard = 'JC';
                } else if(aura.test(ccNumber)) {
                    creditCard = 'AU';
                }

                return creditCard;
            }
        });
    }
);