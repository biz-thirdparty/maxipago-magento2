define(
    [
        'underscore',
        'jquery',
        'ko',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/action/place-order',
        'Magento_Checkout/js/action/select-payment-method',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/checkout-data',
        'Magento_Payment/js/model/credit-card-validation/credit-card-data',
        'Magento_Payment/js/model/credit-card-validation/validator',
        'Magento_Checkout/js/model/payment/additional-validators',
        'mage/url',
        'mage/calendar',
        'mage/translate'
    ],
    function (_,
              $,
              ko,
              quote,
              priceUtils,
              Component,
              placeOrderAction,
              selectPaymentMethodAction,
              customer,
              checkoutData,
              creditCardData,
              validator,
              additionalValidators,
              cardNumberValidator,
              custom,
              url,
              calendar) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'MaxiPago_Payment/payment/maxipago_payment_dc'
            },

            isActive: function () {
                return parseInt(window.checkoutConfig.payment.maxipago_payment_dc.active);
            },

            getDcType: function () {
                return [
                    {"value": "VI", "dc_type": "Visa"},
                    {"value": "MC", "dc_type": "Mastercard"},
                    {"value": "AM", "dc_type": "Amex"},
                    {"value": "DC", "dc_type": "Diners Club"},
                    {"value": "EL", "dc_type": "Elo"},
                    {"value": "DI", "dc_type": "Discover"},
                    {"value": "HC", "dc_type": "Hipercard"},
                    {"value": "JC", "dc_type": "JCB"},
                    {"value": "AU", "dc_type": "Aura"},
                    {"value": "CR", "dc_type": "Credz"}
                ];
            },

            getInstallments: function () {
                return window.checkoutConfig.payment.maxipago_payment_dc.installments;
            },

            getDcYearsValues: function () {
                return _.map(window.checkoutConfig.payment.maxipago_payment_dc.years, function (value, key) {
                    return {
                        'value': key,
                        'year': value
                    };
                });
            },

            getDcMonthsValues: function () {
                return _.map(window.checkoutConfig.payment.maxipago_payment_dc.months, function (value, key) {
                    return {
                        'value': key,
                        'month': value
                    };
                });
            },

            selectPaymentMethod: function () {
                selectPaymentMethodAction(this.getData());
                checkoutData.setSelectedPaymentMethod('maxipago_payment_dc');
                return true;
            },

            getData: function () {
                return {
                    'method': 'maxipago_payment_dc',
                    'additional_data': {
                        'dc_type': jQuery('.maxipago_dc_type').val(),
                        'dc_number': jQuery('#' + this.getCode() + '_dc_number').val(),
                        'dc_owner': jQuery('#' + this.getCode() + '_dc_owner').val(),
                        'month': jQuery('#' + this.getCode() + '_month').val(),
                        'year': jQuery('#' + this.getCode() + '_year').val(),
                        'dc_cid': jQuery('#' + this.getCode() + '_dc_cid').val(),
                        'cpf_cnpj': jQuery('#' + this.getCode() + '_cpf_cnpj').val()
                    }
                };
            },

            getCode: function () {
                return 'maxipago_payment_dc';
            },

            isActiveMaster: function () {
                return window.checkoutConfig.payment.maxipago_payment_dc.active_icons.master;
            },

            isActiveVisa: function () {
                return window.checkoutConfig.payment.maxipago_payment_dc.active_icons.visa;
            },

            getIconMaster: function () {
                return window.checkoutConfig.payment.maxipago_payment_dc.icons.master;
            },

            getIconVisa: function () {
                return window.checkoutConfig.payment.maxipago_payment_dc.icons.visa;
            },

            changeIdentifyDcNumber: function () {
                jQuery('#payment_form_maxipago_payment_dc input.maxipago_dc_type').each(function() {
                    jQuery(this).parent().find('img').css("border", "none");
                    jQuery(this).parent().find('img').css("opacity", "1");
                });

                var dcType = this.identifydcNumber(jQuery("#maxipago_payment_dc_dc_number").val());

                jQuery('#payment_form_maxipago_payment_dc input.maxipago_dc_type').each(function() {
                    if (jQuery(this).val() == dcType) {
                        jQuery(this).click();
                        jQuery(this).parent().find('img').css("border", "1px solid #b30000");
                    } else {
                        jQuery(this).parent().find('img').css("opacity", "0.5");
                    }
                });
            },

            identifydcNumber: function(dcNumber) {
                var creditCard = '';
                var visa = /^4\d{12,15}/;
                var master = /^5[1-5]{1}\d{14}/;
                var amex = /^(34|37)\d{13}/;
                var elo = /^(636368|438935|504175|451416|(6362|5067|4576|4011)\d{2})\d{10}/;
                var discover = /^(6011|622\d{1}|(64|65)\d{2})\d{12}/;
                var hipercard = /^(60\d{2}|3841)\d{9,15}/;
                var diners = /^((30(1|5))|(36|38)\d{1})\d{11}/;
                var jcb = /^(?:2131|1800|35\d{3})\d{11}/;
                var aura = /^50\d{14}/;

                if (visa.test(dcNumber)) {
                    creditCard = 'VI';
                } else if(master.test(dcNumber)) {
                    creditCard = 'MC';
                } else if(amex.test(dcNumber)) {
                    creditCard = 'AM';
                } else if(discover.test(dcNumber)) {
                    creditCard = 'DI';
                } else if(diners.test(dcNumber)) {
                    creditCard = 'DC';
                } else if (elo.test(dcNumber)) {
                    creditCard = 'EL';
                } else if(hipercard.test(dcNumber)) {
                    creditCard = 'HC';
                } else if(jcb.test(dcNumber)) {
                    creditCard = 'JC';
                } else if(aura.test(dcNumber)) {
                    creditCard = 'AU';
                }

                return creditCard;
            }
        });
    }
);