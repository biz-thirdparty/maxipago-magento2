define(
    [
        'underscore',
        'jquery',
        'ko',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'Magento_Checkout/js/view/payment/default',
        'MaxiPago_Payment/js/action/set-payment-method-action',
        'Magento_Checkout/js/action/place-order',
        'Magento_Checkout/js/action/select-payment-method',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/checkout-data',
        'Magento_Payment/js/model/credit-card-validation/credit-card-data',
        'Magento_Payment/js/model/credit-card-validation/validator',
        'Magento_Checkout/js/model/payment/additional-validators',
        'mage/url',
        'mage/calendar',
        'mage/translate'
    ],
    function (_,
              $,
              ko,
              quote,
              priceUtils,
              Component,
              setPaymentMethodAction,
              placeOrderAction,
              selectPaymentMethodAction,
              customer,
              checkoutData,
              creditCardData,
              validator,
              additionalValidators,
              cardNumberValidator,
              custom,
              url,
              calendar
              ) {
        'use strict';

        return Component.extend({
            defaults: {
                redirectAfterPlaceOrder: false,
                template: 'MaxiPago_Payment/payment/maxipago_payment_tef'
            },

            isActive: function () {
                return parseInt(window.checkoutConfig.payment.maxipago_payment_tef.active);
            },

            /** Returns send check to info */
            getDescription: function () {
                return window.checkoutConfig.payment.maxipago_payment_tef.description;
            },

            getBanksActive: function () {
                return 1;
            },

            getBanks: function () {
                var banks = {
                    17: "Bradesco"
                    , 18: "Itaú"
                };

                var newArray = [];

                function logArrayElements(element, index, array) {
                    newArray.push({"value": element, "banks": banks[element]});
                }

                window.checkoutConfig.payment.maxipago_payment_tef.banks.forEach(logArrayElements);

                return newArray;
            },

            selectPaymentMethod: function () {
                selectPaymentMethodAction(this.getData());
                checkoutData.setSelectedPaymentMethod('maxipago_payment_tef');
                return true;
            },

            getData: function () {
                return {
                    'method': 'maxipago_payment_tef',
                    'additional_data': {
                        'bank': jQuery('#' + this.getCode() + '_bank').val(),
                        'cpf_cnpj': jQuery('#' + this.getCode() + '_cpf').val()
                    }
                };
            },

            getCode: function () {
                return 'maxipago_payment_tef';
            },

            afterPlaceOrder: function () {
                setPaymentMethodAction(this.messageContainer);
                return false;
            }
        });
    }
);