/*jshint jquery:true*/
define(
    [
        'jquery',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/url-builder',
        'mage/storage',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/place-order',
        'mage/url'
    ],
    function ($, quote, urlBuilder, storage, errorProcessor, customer, fullScreenLoader, placeOrder, url) {
        'use strict';
        return function (messageContainer) {
            $.mage.redirect(url.build('maxipago/checkout/redirect'));
        };
    }
);