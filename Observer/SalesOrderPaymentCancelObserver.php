<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace MaxiPago\Payment\Observer;

use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;

class SalesOrderPaymentCancelObserver extends AbstractDataAssignObserver
{
    /**
     * @var LoggerInterface|\Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var 
     */
    protected $helper;

    /**
     * @var \MaxiPago\Payment\Helper\Order
     */
    protected $orderHelper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \MaxiPago\Payment\Helper\Api
     */
    protected $apiHelper;

    /**
     * SalesOrderPaymentCancelObserver constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param \MaxiPago\Payment\Helper\Data $helper
     * @param \MaxiPago\Payment\Helper\Order $orderHelper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \MaxiPago\Payment\Helper\Api $apiHelper
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \MaxiPago\Payment\Helper\Data $helper,
        \MaxiPago\Payment\Helper\Order $orderHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \MaxiPago\Payment\Helper\Api $apiHelper
    )
    {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->orderHelper = $orderHelper;
        $this->scopeConfig = $scopeConfig;
        $this->apiHelper = $apiHelper;
    }

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        try {
            $order = $observer->getEvent()->getPayment()->getOrder();

            $payment = $order->getPayment();
            $methodCode = $payment->getMethod();

            if ($methodCode == 'maxipago_payment_cc') {
                $cancelled = $payment->getAdditionalInformation('cancelled');

                if (!$cancelled) {
                    $captured = $payment->getAdditionalInformation('captured');
                    $maxipagoOrderId = $payment->getAdditionalInformation('order_id');
                    $transactionId = $payment->getAdditionalInformation('transaction_id');

                    if ($captured) {
                        $amount = ($order->getBaseTotalInvoiced()) ? $order->getBaseTotalInvoiced() : $order->getBaseGrandTotal();

                        $capturedDate = $payment->getAdditionalInformation('captured_date');
                        $currentDate = new \DateTime();
                        $currentDate = $currentDate->format('Y-m-d');

                        if ($captured && $capturedDate == $currentDate) {
                            $this->apiHelper->void($order, $maxipagoOrderId);
                        } else {
                            $this->apiHelper->refund($order, $maxipagoOrderId, $amount);
                        }
                    } else {
                        $this->apiHelper->void($order, $transactionId);
                    }

                    $payment->setAdditionalInformation('cancelled', true);
                    $payment->save();
                }

            } else if ($methodCode == 'maxipago_payment_checkout2') {

                $payOrderId = $payment->getAdditionalInformation('pay_order_id');
                $this->apiHelper->cancelPaymentOrder($order, $payOrderId);

                $payment->setAdditionalInformation('cancelled', true);
                $payment->save();

            }

        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}