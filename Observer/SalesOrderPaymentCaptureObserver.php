<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace MaxiPago\Payment\Observer;

use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;

class SalesOrderPaymentCaptureObserver extends AbstractDataAssignObserver
{
    /**
     * @var LoggerInterface|\Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var 
     */
    protected $helper;

    /**
     * SalesOrderPaymentPlaceEndObserver constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param \MaxiPago\Payment\Helper\Data $helper
     * @param \MaxiPago\Payment\Helper\Order $orderHelper
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \MaxiPago\Payment\Helper\Data $helper
    )
    {
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $payment = $observer->getEvent()->getPayment();

        $invoice = $observer->getEvent()->getInvoice();
        $invoice->setTransactionId($payment->getAdditionalInformation('transaction_id'));
    }
}