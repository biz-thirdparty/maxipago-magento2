<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace MaxiPago\Payment\Observer;

use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;

class SalesOrderPaymentPlaceEndObserver extends AbstractDataAssignObserver
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \MaxiPago\Payment\Helper\Data
     */
    protected $helper;

    /**
     * @var \MaxiPago\Payment\Helper\Order
     */
    protected $orderHelper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * SalesOrderPaymentPlaceEndObserver constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param \MaxiPago\Payment\Helper\Data $helper
     * @param \MaxiPago\Payment\Helper\Order $orderHelper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \MaxiPago\Payment\Helper\Data $helper,
        \MaxiPago\Payment\Helper\Order $orderHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->orderHelper = $orderHelper;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $payment = $observer->getEvent()->getPayment();

        $availableMethods = $this->helper->getAvailableMethods();
        $status = false;

        $methodCode = $payment->getMethod();
        $message = '';

        if (in_array($methodCode, $availableMethods)) {
            $order = $payment->getOrder();

            $responseCode = $payment->getAdditionalInformation('response_code');
            $responseMessage = $payment->getAdditionalInformation('response_message');
            $tid = $payment->getAdditionalInformation('transaction_id');

            $canCancel  = $this->scopeConfig->getValue('payment/maxipago_payment/automatically_cancel');
            $canInvoice = false;

            if ($responseCode == 0) {
                if ($methodCode == 'maxipago_payment_cc') {
                    $paymentAction = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/cc_payment_action');
                    $authStatus = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/authorized_order_status');
                    $saleStatus = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/captured_order_status');

                    $message = __('The payment was authorized - Transaction ID: ') . (string) $tid;

                    if ($paymentAction == 'sale') {
                        $canInvoice = true;
                        $status = $saleStatus;
                    } else {
                        if ($responseMessage == 'CAPTURED') {
                            $canInvoice = $this->scopeConfig->getValue('payment/maxipago_payment/maxipago_payment_cc/capture_on_low_risk');
                        }

                        $status = $authStatus;
                    }
                }
            } elseif ($responseCode != 5) {
                $message = __('The payment was\'t authorized - Transaction ID: %s') . (string) $tid;

                if ($canCancel) {
                    if ($order->canCancel()) {
                        $order->cancel();
                    }
                }
            }

            if ($canInvoice) {
                $order->save();
                $this->orderHelper->createInvoice($order, $tid);
            }

            if ($message) {
                $order->addStatusHistoryComment($message, $status)->setIsCustomerNotified(true);
                $order->save();
            }
        }
    }
}