<?php

namespace MaxiPago\Payment\Controller\Notifications;

class Error extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \MaxiPago\Payment\Helper\Api
     */
    protected $helperApi;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * @var \MaxiPago\Payment\Helper\Order
     */
    protected $helperOrder;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Framework\Event\Manager
     */
    protected $eventManager;

    /**
     * Error constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \MaxiPago\Payment\Helper\Api $helperApi
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \MaxiPago\Payment\Helper\Order $helperOrder
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\Event\Manager $eventManager
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Psr\Log\LoggerInterface $logger,
        \MaxiPago\Payment\Helper\Api $helperApi,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \MaxiPago\Payment\Helper\Order $helperOrder,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Event\Manager $eventManager
    )
    {
        parent::__construct($context);
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->logger = $logger;
        $this->helperApi = $helperApi;
        $this->orderFactory = $orderFactory;
        $this->helperOrder = $helperOrder;
        $this->_checkoutSession = $checkoutSession;
        $this->eventManager = $eventManager;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $this->logParams();

        if (!$lastOrderId = $this->_getLastOrderId()) {
            $this->_redirectCart();
            return;
        }

        try {
            $this->_updateOrder();
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        $this->_redirectComplete();
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _updateOrder()
    {
        $orderId = $this->getRequest()->getParam('hp_orderid');

        if ($orderId) {
            $response = $this->helperApi->pullReportByOrderId($orderId);

            $record = isset($response['records'][0]) ? $response['records'][0] : null;

            if ($record) {
                $incrementId = isset($record['referenceNumber']) ? $record['referenceNumber'] : null;

                $order = $this->orderFactory->create()->loadByIncrementId($incrementId);

                if ($order->getId()) {
                    $this->helperOrder->updatePayment($order, $record);
                }
            }

        }
    }

    /**
     * @return $this
     */
    protected function _redirectCart()
    {
        $this->_redirect('checkout/cart');
        return $this;
    }

    /**
     * @return $this
     */
    protected function _redirectComplete()
    {
        $this->_redirect('checkout/onepage/success');
        return $this;
    }

    /**
     * @return int
     */
    protected function _getLastOrderId()
    {
        $lastOrderId = $this->_checkoutSession->getLastOrderId();

        if (empty($lastOrderId)) {
            $lastOrderId = $this->_checkoutSession->getLastOrderId();
        }

        return (int) $lastOrderId;
    }

    protected function logParams()
    {
        $this->logger->info($this->getRequest()->getOriginalPathInfo());
        $this->logger->info('Params');
        $this->logger->info(json_encode($this->getRequest()->getParams()));
        $this->logger->info($this->getRequest()->getRawBody());
    }
}