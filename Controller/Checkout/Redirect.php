<?php

namespace MaxiPago\Payment\Controller\Checkout;

class Redirect extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * Redirect constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
    )
    {
        parent::__construct($context);
        $this->orderCollectionFactory = $orderCollectionFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $orderCollection = $this->orderCollectionFactory->create()->addFieldToSelect(array('*'));
        $orderCollection->setOrder('entity_id','DESC');

        $order = $orderCollection->getFirstItem();

        $payment = $order->getPayment();

        $url = $payment->getAdditionalInformation('maxipago_redirect_url');

        if ($url) {
            $this->getResponse()->setRedirect($url);
            return;
        }

        $this->_redirect('checkout/onepage/success');
    }
}